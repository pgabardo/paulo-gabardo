FROM openjdk:8-jdk-slim
VOLUME /tmp
USER root
ENV TZ=America/Sao_Paulo
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN mkdir /var/log/paulo-gabardo-api
ADD /target/paulo-gabardo-api.jar paulo-gabardo-api.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/paulo-gabardo-api.jar"]