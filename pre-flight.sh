#!/usr/local/bin/bash
. PROJETO.cfg

echo "Instalando as dependencias 1/2..."
apk add curl
apk add jq

echo ${CI_API_V4_URL}
echo ${PROJECT_NAMESPACE}

echo "Verificando se o namespace existe..."
NAMESPACE_ID=`curl -H "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" -s \
	"${CI_API_V4_URL}/namespaces?search=${PROJECT_NAMESPACE}" |  jq '.[].id' `

if [[ -z "$NAMESPACE_ID" ]]; then
	echo "Não foi encontrado o NAMESPACE ${PROJECT_NAMESPACE}. Aborting."
	exit 1
fi

echo "Instalando as dependencias 2/2..."
apk add git
apk add sed

echo "PRE-FLIGHT OK"

exit 0
