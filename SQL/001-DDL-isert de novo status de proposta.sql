-- create user SEBRAETEC_CON IDENTIFIED BY "SEBRAETEC_CON"
--  default tablespace USERS
--  temporary tablespace TEMP
--  profile DEFAULT;
-- grant CONNECT to SEBRAETEC_CON;
-- grant RESOURCE to SEBRAETEC_CON;
-- grant UNLIMITED TABLESPACE to SEBRAETEC_CON;
--
create table SEBRAETEC_DATA.TB000_example
(
    --br.com.wise.boot.sebrae.entity.SebraeBaseEntity
    A0015_COD_AGENTE_INC number                            not null,
    A0015_COD_AGENTE_ATZ number                            not null,
    --br.com.wise.boot.starter.entity.base.BaseEntity
    A000_DT_INC          date          default SYSDATE     not null,
    A000_DT_ATZ          date          default SYSDATE     not null,
    A000_IND_ATIVO       number(1)     default 1           not null,
    A000_COD_VERSAO      number        default 0           not null,
    A000_COD_EMPRESA     varchar2(255) default 'SEBRAE-PR' not null,
    --
    a000_cod_example     number                            not null,
    a000_cod_example_pai number                            null,
    a000_nme_example     varchar2(255)                     not null
)
    tablespace COMPARESE_DATA
        storage
(
    initial 64 K
    minextents 1
    maxextents unlimited
);

-- Create sequence
create sequence SEBRAETEC_DATA.SQ_example
    start with 1
    increment by 1
    cache 20;