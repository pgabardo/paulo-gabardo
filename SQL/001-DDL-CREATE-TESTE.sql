insert into master.tb0939_st2_prop_status
       (a0939_cod_status,
        a0939_dsc_status,
        a0939_label_status,
        a0015_cod_agente_inc,
        a0015_cod_agente_atz,
        a0939_dt_inc,
        a0939_dt_atz,
        a0939_ind_ativo,
        a0939_ind_solicitante,
        a0939_ind_responsavel,
        a0939_ind_gerente)
        values
        ('10,5',
         'Recusado pelo cliente [responsável/solicitante/gerente]',
         'Recusado pelo cliente aguardando cancelamento',
         9543152,
         9543152,
         sysdate,
         sysdate,
         1,
         1,
         1,
         1);
commit;   
