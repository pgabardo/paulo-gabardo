package br.com.sebrae.pr.sebraetec.api.dto;

import java.util.Date;

import br.com.wise.boot.starter.dto.base.LongIdBaseDTO;

public class AtividadePropostaDTO extends LongIdBaseDTO {

	private static final long serialVersionUID = -5726398863199195674L;
	
	private Long codProposta;
	private Long codAgenteEnt;
	private Long codAtividade;
	private Long codDomin;
	private String dscDomin;
	private String nmAtividade;
	private String nmAtividadeSemAspas;
	private String dscStividade;
	private String vlrHora;
	private String vlrHoraFm;
	private String vlrHoraFme;
	private Long qntHora;
	private String vlrTotalFm;
	private Date dtInicio;
	private String dtInicioFm;
	private Date dtFim;
	private String dtFimFm;
	private String dtTarefaFm;
	private String dtTarefaOrigFm;
	private Long codAgenteInc;
	private Long codAgenteAtz;
	private Date dtInc;
	private Date dtAtz;
	private String dtAtzFm;
	private String dtIncFm;
	private String nmAgenteAtz;
	private String nmAgenteInc;
	private String dtTarefAprorFm;
	private Date dtInicioAprov;
	private String dtInicioAprovFm;
	private Date dtFimAprov;
	private String dtFimAprovFm;
	private Date dtSolicAprov;
	private String dtSolicAprovFm;
	private Long porcConcluido;
	private String porcConcluidoFm;
	private String dtInicioEdicaoFm;
	private String dtFimEdicaoFm;
	private boolean indConcluido;
	private boolean indAtivo;
	private boolean indEditarAtiv; 

    public AtividadePropostaDTO() {
    }

	public Long getCodProposta() {
		return codProposta;
	}

	public void setCodProposta(Long codProposta) {
		this.codProposta = codProposta;
	}

	public Long getCodAgenteEnt() {
		return codAgenteEnt;
	}

	public void setCodAgenteEnt(Long codAgenteEnt) {
		this.codAgenteEnt = codAgenteEnt;
	}

	public Long getCodAtividade() {
		return codAtividade;
	}

	public void setCodAtividade(Long codAtividade) {
		this.codAtividade = codAtividade;
	}

	public Long getCodDomin() {
		return codDomin;
	}

	public void setCodDomin(Long codDomin) {
		this.codDomin = codDomin;
	}

	public String getDscDomin() {
		return dscDomin;
	}

	public void setDscDomin(String dscDomin) {
		this.dscDomin = dscDomin;
	}

	public String getNmAtividade() {
		return nmAtividade;
	}

	public void setNmAtividade(String nmAtividade) {
		this.nmAtividade = nmAtividade;
	}

	public String getNmAtividadeSemAspas() {
		return nmAtividadeSemAspas;
	}

	public void setNmAtividadeSemAspas(String nmAtividadeSemAspas) {
		this.nmAtividadeSemAspas = nmAtividadeSemAspas;
	}

	public String getDscStividade() {
		return dscStividade;
	}

	public void setDscStividade(String dscStividade) {
		this.dscStividade = dscStividade;
	}

	public String getVlrHora() {
		return vlrHora;
	}

	public void setVlrHora(String vlrHora) {
		this.vlrHora = vlrHora;
	}

	public String getVlrHoraFm() {
		return vlrHoraFm;
	}

	public void setVlrHoraFm(String vlrHoraFm) {
		this.vlrHoraFm = vlrHoraFm;
	}

	public String getVlrHoraFme() {
		return vlrHoraFme;
	}

	public void setVlrHoraFme(String vlrHoraFme) {
		this.vlrHoraFme = vlrHoraFme;
	}

	public Long getQntHora() {
		return qntHora;
	}

	public void setQntHora(Long qntHora) {
		this.qntHora = qntHora;
	}

	public String getVlrTotalFm() {
		return vlrTotalFm;
	}

	public void setVlrTotalFm(String vlrTotalFm) {
		this.vlrTotalFm = vlrTotalFm;
	}

	public Date getDtInicio() {
		return dtInicio;
	}

	public void setDtInicio(Date dtInicio) {
		this.dtInicio = dtInicio;
	}

	public String getDtInicioFm() {
		return dtInicioFm;
	}

	public void setDtInicioFm(String dtInicioFm) {
		this.dtInicioFm = dtInicioFm;
	}

	public Date getDtFim() {
		return dtFim;
	}

	public void setDtFim(Date dtFim) {
		this.dtFim = dtFim;
	}

	public String getDtFimFm() {
		return dtFimFm;
	}

	public void setDtFimFm(String dtFimFm) {
		this.dtFimFm = dtFimFm;
	}

	public String getDtTarefaFm() {
		return dtTarefaFm;
	}

	public void setDtTarefaFm(String dtTarefaFm) {
		this.dtTarefaFm = dtTarefaFm;
	}

	public String getDtTarefaOrigFm() {
		return dtTarefaOrigFm;
	}

	public void setDtTarefaOrigFm(String dtTarefaOrigFm) {
		this.dtTarefaOrigFm = dtTarefaOrigFm;
	}

	public Long getCodAgenteInc() {
		return codAgenteInc;
	}

	public void setCodAgenteInc(Long codAgenteInc) {
		this.codAgenteInc = codAgenteInc;
	}

	public Long getCodAgenteAtz() {
		return codAgenteAtz;
	}

	public void setCodAgenteAtz(Long codAgenteAtz) {
		this.codAgenteAtz = codAgenteAtz;
	}

	public Date getDtInc() {
		return dtInc;
	}

	public void setDtInc(Date dtInc) {
		this.dtInc = dtInc;
	}

	public Date getDtAtz() {
		return dtAtz;
	}

	public void setDtAtz(Date dtAtz) {
		this.dtAtz = dtAtz;
	}

	public String getDtAtzFm() {
		return dtAtzFm;
	}

	public void setDtAtzFm(String dtAtzFm) {
		this.dtAtzFm = dtAtzFm;
	}

	public String getDtIncFm() {
		return dtIncFm;
	}

	public void setDtIncFm(String dtIncFm) {
		this.dtIncFm = dtIncFm;
	}

	public String getNmAgenteAtz() {
		return nmAgenteAtz;
	}

	public void setNmAgenteAtz(String nmAgenteAtz) {
		this.nmAgenteAtz = nmAgenteAtz;
	}

	public String getNmAgenteInc() {
		return nmAgenteInc;
	}

	public void setNmAgenteInc(String nmAgenteInc) {
		this.nmAgenteInc = nmAgenteInc;
	}

	public String getDtTarefAprorFm() {
		return dtTarefAprorFm;
	}

	public void setDtTarefAprorFm(String dtTarefAprorFm) {
		this.dtTarefAprorFm = dtTarefAprorFm;
	}

	public Date getDtInicioAprov() {
		return dtInicioAprov;
	}

	public void setDtInicioAprov(Date dtInicioAprov) {
		this.dtInicioAprov = dtInicioAprov;
	}

	public String getDtInicioAprovFm() {
		return dtInicioAprovFm;
	}

	public void setDtInicioAprovFm(String dtInicioAprovFm) {
		this.dtInicioAprovFm = dtInicioAprovFm;
	}

	public Date getDtFimAprov() {
		return dtFimAprov;
	}

	public void setDtFimAprov(Date dtFimAprov) {
		this.dtFimAprov = dtFimAprov;
	}

	public String getDtFimAprovFm() {
		return dtFimAprovFm;
	}

	public void setDtFimAprovFm(String dtFimAprovFm) {
		this.dtFimAprovFm = dtFimAprovFm;
	}

	public Date getDtSolicAprov() {
		return dtSolicAprov;
	}

	public void setDtSolicAprov(Date dtSolicAprov) {
		this.dtSolicAprov = dtSolicAprov;
	}

	public String getDtSolicAprovFm() {
		return dtSolicAprovFm;
	}

	public void setDtSolicAprovFm(String dtSolicAprovFm) {
		this.dtSolicAprovFm = dtSolicAprovFm;
	}

	public Long getPorcConcluido() {
		return porcConcluido;
	}

	public void setPorcConcluido(Long porcConcluido) {
		this.porcConcluido = porcConcluido;
	}

	public String getPorcConcluidoFm() {
		return porcConcluidoFm;
	}

	public void setPorcConcluidoFm(String porcConcluidoFm) {
		this.porcConcluidoFm = porcConcluidoFm;
	}

	public String getDtInicioEdicaoFm() {
		return dtInicioEdicaoFm;
	}

	public void setDtInicioEdicaoFm(String dtInicioEdicaoFm) {
		this.dtInicioEdicaoFm = dtInicioEdicaoFm;
	}

	public String getDtFimEdicaoFm() {
		return dtFimEdicaoFm;
	}

	public void setDtFimEdicaoFm(String dtFimEdicaoFm) {
		this.dtFimEdicaoFm = dtFimEdicaoFm;
	}

	public boolean isIndConcluido() {
		return indConcluido;
	}

	public void setIndConcluido(boolean indConcluido) {
		this.indConcluido = indConcluido;
	}

	public boolean isIndAtivo() {
		return indAtivo;
	}

	public void setIndAtivo(boolean indAtivo) {
		this.indAtivo = indAtivo;
	}

	public boolean isIndEditarAtiv() {
		return indEditarAtiv;
	}

	public void setIndEditarAtiv(boolean indEditarAtiv) {
		this.indEditarAtiv = indEditarAtiv;
	}
    	
}
