package br.com.sebrae.pr.sebraetec.api.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;

import br.com.sebrae.pr.api.certisign.dto.create.CreateRequestDTO;
import br.com.sebrae.pr.api.certisign.dto.create.CreateResponseDTO;
import br.com.sebrae.pr.api.certisign.dto.create.Document;
import br.com.sebrae.pr.api.certisign.dto.create.ElectronicSigner;
import br.com.sebrae.pr.api.certisign.dto.create.Sender;
import br.com.sebrae.pr.api.certisign.dto.create.Upload;
import br.com.sebrae.pr.api.certisign.dto.upload.UploadRequestDTO;
import br.com.sebrae.pr.api.certisign.dto.upload.UploadResponseDTO;
import br.com.sebrae.pr.api.certisign.service.CertisignApiClientService;
import br.com.sebrae.pr.sebraetec.api.dao.CertisignDAO;
import br.com.sebrae.pr.sebraetec.api.dto.Agente;
import br.com.sebrae.pr.sebraetec.api.dto.ArquivoPropostaStecVO;
import br.com.sebrae.pr.sebraetec.api.dto.RelatorioVO;
import br.com.sebrae.pr.sebraetec.api.service.CertisignService;
import br.com.sebrae.pr.sebraetec.api.util.PropertyUtil;


@Service
class CertisignServiceImpl implements CertisignService {
	
	@Autowired
    private CertisignDAO certisignDAO;
	
	Logger logger = LoggerFactory.getLogger(CertisignServiceImpl.class);

	@Override
	public ArquivoPropostaStecVO envioAssinaturaCertisignApi(String auth, RelatorioVO relatorio, ArquivoPropostaStecVO arquivo) throws Exception{
		
    	
    	String endpoint = PropertyUtil.getProperty("endpoint.api.certisign");
    	CreateRequestDTO create    = new CreateRequestDTO();
		CreateResponseDTO response = new CreateResponseDTO();  
		
		arquivo = gerarArquivo(new Long(arquivo.getCodigoProposta()), new Long(arquivo.getCodigoCliente()));
		
		String str = Base64.encodeBase64String(arquivo.getAnexo());
    	UploadRequestDTO file = new UploadRequestDTO(arquivo.getNomeArquivo(), str);
		UploadResponseDTO upresponse = CertisignApiClientService.sendUpload(file,endpoint,auth);
		Document document = new Document("Termo de contrato Sebraetec "+relatorio.getCodigoProposta(), new Upload(upresponse.getUploadId(), arquivo.getNomeArquivo()));
		create.setDocument(document);
		List<ElectronicSigner> electronicSigners = new ArrayList<ElectronicSigner>();
		Agente gestorResp = relatorio.getResponsavel();
		create.setSender(new Sender(gestorResp.getNome(),gestorResp.getEmail(),gestorResp.getCpf()));
		Agente contatoEntidade = relatorio.getContatoEntidade();
		electronicSigners.add(new ElectronicSigner(1,"Entidade",contatoEntidade.getNome(),contatoEntidade.getEmail(),contatoEntidade.getCpf()));
		Agente gerente = relatorio.getGerente();
        electronicSigners.add(new ElectronicSigner(1,"Gerente",gerente.getNome(),gerente.getEmail(),gerente.getCpf()));
        Agente subGerente = relatorio.getSubGerente();
        if (StringUtils.isNotBlank(subGerente.getCodigoAgente())) {
        	electronicSigners.add(new ElectronicSigner(1,"SubGerente",subGerente.getNome(),subGerente.getEmail(),subGerente.getCpf()));        	
        }
        create.setNotifiedList(new br.com.sebrae.pr.api.certisign.dto.create.DocumentNotifications(gestorResp.getEmail() + "," + relatorio.getSolicitante().getEmail()));
		create.setElectronicSigners(electronicSigners);
		response = CertisignApiClientService.sendDocument(create,endpoint,auth);
		arquivo.setIdCertisign(response.getId()+"");
        arquivo.setChaveCertisign(response.getChave());
        
    	return arquivo;
    	
    }
	
	private ArquivoPropostaStecVO gerarArquivo(Long codigoproposta, Long codigocliente) throws Exception {
		
		String TERMO_CONCLUSAO = "1452";
		
		ArquivoPropostaStecVO arquivo = new ArquivoPropostaStecVO();
	    arquivo.setCodigoProposta(codigoproposta.toString());
	    arquivo.setCodigoCliente(codigocliente.toString());
	    arquivo.setCodigoTipoTermo(TERMO_CONCLUSAO); 
	    
	    String urlSoeSebraePr = PropertyUtil.getProperty("url.sebraepr");
	    
	    String urlRelatorio = this.getURLRelatorio(codigoproposta.toString(), TERMO_CONCLUSAO, urlSoeSebraePr, codigocliente.toString());
	    
	    arquivo.setNomeArquivo(this.getNomeArquivo(codigoproposta.toString(), TERMO_CONCLUSAO, codigocliente.toString()));
	    String imgPath = urlSoeSebraePr + "/img";
	    arquivo = getArquivoFromJsp(arquivo, urlRelatorio, imgPath);
	    arquivo.setCodigoStatusTermo("2341"); // Colocar como contrato gerado para poder ser enviado
	    
	    return certisignDAO.setTermoProvisorio(arquivo, codigocliente.toString());
	    
	}
	
	private String getURLRelatorio(String codigoProposta, String codigoTipoContrato, String urlSOE, String codigoCliente) {
        String parametros = "codigoProposta=" + codigoProposta;
        if (codigoCliente != null) {
            parametros = parametros + "&codigoCliente=" + codigoCliente;
        }
        try {
            switch (Integer.parseInt(codigoTipoContrato)) {
                case 1451:
                    return urlSOE + "/GoRelatorioPactoStec.do?" + parametros;
                case 2335:
                    return urlSOE + "/GoRelatorioPactoAgregadoStec.do?" + parametros;
                case 1452:
                    return urlSOE + "/GoRelatorioFinalizacaoStec.do?" + parametros;
                case 2336:
                    return urlSOE + "/GoRelatorioFinalizacaoAgregadoStec.do?" + parametros;
                case 2337:
                    return urlSOE + "/GoRelatorioCursoStec.do?" + parametros;
                default:
                    return "";
            }
        } catch (Exception e) {
            return "";
        }

    }
	
	private String getNomeArquivo(String codigoProposta, String codigoTipoContrato, String codigoCliente) {
        try {
            switch (Integer.parseInt(codigoTipoContrato)) {
                case 1451:
                    return "Termo_pacto_" + codigoProposta + ((codigoCliente != null) ? "_" + codigoCliente : "") + ".pdf";
                case 2335:
                    return "Termo_pacto_coletivo_" + codigoProposta + ((codigoCliente != null) ? "_" + codigoCliente : "") + ".pdf";
                case 1452:
                    return "Termo_finaliz_" + codigoProposta + ((codigoCliente != null) ? "_" + codigoCliente : "") + ".pdf";
                case 2336:
                    return "Termo_finaliz_coletivo_" + codigoProposta + ((codigoCliente != null) ? "_" + codigoCliente : "") + ".pdf";
                case 2337:
                    return "Termo_curso_" + codigoProposta + ((codigoCliente != null) ? "_" + codigoCliente : "") + ".pdf";
                default:
                    return "Termo_" + codigoProposta + ((codigoCliente != null) ? "_" + codigoCliente : "") + ".pdf";
            }
        } catch (Exception e) {
            return "Termo_" + codigoProposta + ((codigoCliente != null) ? "_" + codigoCliente : "") + ".pdf";
        }

    }
	
	private ArquivoPropostaStecVO getArquivoFromJsp(ArquivoPropostaStecVO arquivo, String urlRelatorio, String imgPath) {
        arquivo.setAnexo(getByteArrayFromJsp(urlRelatorio, imgPath));
        return arquivo;
    }
	
	private byte[] getByteArrayFromJsp(String urlRelatorio, String imgPath) {
        byte bytearray[] = null;
        PdfWriter pdfWriter = null;
        com.itextpdf.text.Document document = new com.itextpdf.text.Document();
        // open document
        InputStreamReader fis = null;
        try {
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            pdfWriter = PdfWriter.getInstance(document, byteStream);

            URL myWebPage = new URL(urlRelatorio);
            fis = new InputStreamReader(myWebPage.openStream(), "UTF-8");
            document.addAuthor("SebraePR");
            document.addCreationDate();
            document.addProducer();
            document.addCreator("http://sebraepr.com.br");
            document.addTitle("Sebrae");
            document.setPageSize(PageSize.A4);
            document.open();

            Image logoSebraetec = Image.getInstance(imgPath + "/logo_sebraetec.png");
            logoSebraetec.setAbsolutePosition(10f, 753f);
            // logoSebraetec.setAbsolutePosition(10f, 733f);
            document.add(logoSebraetec);

            // get the XMLWorkerHelper Instance
            XMLWorkerHelper worker = XMLWorkerHelper.getInstance();

            // convert to PDF
            worker.parseXHtml(pdfWriter, document, fis);
            // close the document
            document.close();
            // close the writer
            pdfWriter.close();

            bytearray = byteStream.toByteArray();
            byteStream.close();

        } catch (FileNotFoundException e) {
        	logger.error("[CertisignServiceImpl.getByteArrayFromJsp] ação (ENVIAR_ASSINATURA), metodo (getByteArray) FileNotFoundException - Mensagem:");
        } catch (DocumentException e) {
        	logger.error("[CertisignServiceImpl.getByteArrayFromJsp] ação (ENVIAR_ASSINATURA), metodo (getByteArray) DocumentException - Mensagem:");
        } catch (MalformedURLException e) {
        	logger.error("[CertisignServiceImpl.getByteArrayFromJsp] ação (ENVIAR_ASSINATURA), metodo (getByteArray) MalformedURLException - Mensagem:");
        } catch (IOException e) {
        	logger.error("[CertisignServiceImpl.getByteArrayFromJsp] ação (ENVIAR_ASSINATURA), metodo (getByteArray) IOException - Mensagem:");
        }

        return bytearray;
    }
	
}
