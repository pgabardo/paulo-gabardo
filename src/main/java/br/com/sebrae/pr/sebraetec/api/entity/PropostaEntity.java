package br.com.sebrae.pr.sebraetec.api.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.wise.boot.sebrae.entity.SebraeBaseEntity;

@Entity
@Table(schema = "BANCOPERGUNTAS", name = "TB027_AGENTE_EDITOR")
@AttributeOverrides({ //
		@AttributeOverride(name = "dataInclusao", column = @Column(name = "A027_DT_INC")), //
		@AttributeOverride(name = "dataAtualizacao", column = @Column(name = "A027_DT_ATZ")), //
		@AttributeOverride(name = "ativo", column = @Column(name = "A027_IND_ATIVO"))//
})
public class PropostaEntity extends SebraeBaseEntity<Long> {
	private static final long serialVersionUID = 3008323819097006733L;

	@Id
	private Long cod;

	public PropostaEntity() {
		super();
	}

	public Long getCod() {
		return cod;
	}

	public void setCod(Long cod) {
		this.cod = cod;
	}

}
