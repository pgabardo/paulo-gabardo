package br.com.sebrae.pr.sebraetec.api.service;

import br.com.sebrae.pr.sebraetec.api.dto.ArquivoPropostaStecVO;
import br.com.sebrae.pr.sebraetec.api.dto.RelatorioVO;

public interface CertisignService{
	
	public ArquivoPropostaStecVO envioAssinaturaCertisignApi(String auth,RelatorioVO relatorio, ArquivoPropostaStecVO arquivo) throws Exception;

}
