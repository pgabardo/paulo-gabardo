package br.com.sebrae.pr.sebraetec.api.service;

import java.util.List;

import br.com.sebrae.pr.sebraetec.api.dto.AtividadePropostaDTO;
import br.com.sebrae.pr.sebraetec.api.dto.PropostaDTO;
import br.com.wise.boot.sebrae.dto.filtro.FiltroBaseLong;
import br.com.wise.boot.starter.service.BaseCrudService;
import br.com.wise.boot.starter.service.BasePagedService;

public interface PropostaService extends BasePagedService<FiltroBaseLong, PropostaDTO>, BaseCrudService<Long, PropostaDTO> {
	
	public List<PropostaDTO> pesquisarPorCodigoCliente(Long codCliente);
	
	public List<AtividadePropostaDTO> pesquisarAtividadesProposta(Long codProposta); 
	
	public void recusarProposta(Long codProposta, Long codCliente, String justificativa);
    
    public void aprovarProposta(Long codProposta, Long codCliente);
    
    public void aprovarPlanoTrabalho(Long codProposta, Long codCliente, boolean finalizacao);
    
    public void recusarPlanoTrabalho(Long codProposta, Long codCliente, String comentario);

}
