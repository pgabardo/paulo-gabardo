package br.com.sebrae.pr.sebraetec.api.repository;

import br.com.sebrae.pr.sebraetec.api.entity.PropostaEntity;
import br.com.wise.boot.starter.repository.BaseCrudRepository;

public interface PropostaRepository extends BaseCrudRepository<Long, PropostaEntity> {

}
