package br.com.sebrae.pr.sebraetec.api.dto;

public class ResumoFinanceiroVO {

    private String codigoProposta = "";
    private String valorProposta = "";
    private int quantidadeCliente = 0;
    private int quantidadeClientePagante = 0;
    private String valorSebrae = "";
    private String valorSebraeNA = "";
    private String valorParceiro = "";
    private String valorPatrocinador = "";
    private String valorCliente = "";
    private String valorContrapartida = "";
    private String valorFixo = "";
    private String valorSaldo = "";
    private String valorSaldoPrevisto = "";

    public ResumoFinanceiroVO() {
        super();
    }

    public void setCodigoProposta(String codigoProposta) {
        this.codigoProposta = codigoProposta;
    }

    public String getCodigoProposta() {
        return codigoProposta;
    }

    public void setValorProposta(String valorProposta) {
        this.valorProposta = valorProposta;
    }

    public String getValorProposta() {
        return valorProposta;
    }

    public void setQuantidadeCliente(int quantidadeCliente) {
        this.quantidadeCliente = quantidadeCliente;
    }

    public int getQuantidadeCliente() {
        return quantidadeCliente;
    }

    public void setQuantidadeClientePagante(int quantidadeClientePagante) {
        this.quantidadeClientePagante = quantidadeClientePagante;
    }

    public int getQuantidadeClientePagante() {
        return quantidadeClientePagante;
    }

    public void setValorSebrae(String valorSebrae) {
        this.valorSebrae = valorSebrae;
    }

    public String getValorSebrae() {
        return valorSebrae;
    }

    public void setValorCliente(String valorCliente) {
        this.valorCliente = valorCliente;
    }

    public String getValorCliente() {
        return valorCliente;
    }

    public void setValorContrapartida(String valorContrapartida) {
        this.valorContrapartida = valorContrapartida;
    }

    public String getValorContrapartida() {
        return valorContrapartida;
    }

    public String getValorSebraeNA() {
        return valorSebraeNA;
    }

    public void setValorSebraeNA(String valorSebraeNA) {
        this.valorSebraeNA = valorSebraeNA;
    }

    public String getValorParceiro() {
        return valorParceiro;
    }

    public void setValorParceiro(String valorParceiro) {
        this.valorParceiro = valorParceiro;
    }

    public String getValorPatrocinador() {
        return valorPatrocinador;
    }

    public void setValorPatrocinador(String valorPatrocinador) {
        this.valorPatrocinador = valorPatrocinador;
    }

    public String getValorFixo() {
        return valorFixo;
    }

    public void setValorFixo(String valorFixo) {
        this.valorFixo = valorFixo;
    }

    public String getValorSaldo() {
        return valorSaldo;
    }

    public void setValorSaldo(String valorSaldo) {
        this.valorSaldo = valorSaldo;
    }

    public String getValorSaldoPrevisto() {
        return valorSaldoPrevisto;
    }

    public void setValorSaldoPrevisto(String valorSaldoPrevisto) {
        this.valorSaldoPrevisto = valorSaldoPrevisto;
    }

}
