package br.com.sebrae.pr.sebraetec.api.dao.impl;

import java.io.ByteArrayInputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import org.springframework.stereotype.Service;

import br.com.sebrae.pr.sebraetec.api.dao.CertisignDAO;
import br.com.sebrae.pr.sebraetec.api.dao.SebraetecPRBaseDao;
import br.com.sebrae.pr.sebraetec.api.dto.ArquivoPropostaStecVO;
import br.com.wise.common.exception.BadRequestException;
import oracle.jdbc.OracleTypes;

@Service
public class CertisignDAOImpl extends SebraetecPRBaseDao implements CertisignDAO {

    @Override
    public ArquivoPropostaStecVO setTermoProvisorio(ArquivoPropostaStecVO doc, String codigoAgenteAtz) throws Exception {

        CallableStatement cstmt = null;
        Connection conn = this.getConnection();
        conn.setAutoCommit(false);
        String sql;

        try {

            sql = "begin  MASTER.PC_STEC_PROPOSTA.SP_MANTEM_TERMO_PROVISORIO(?,?,?,?,?,?,?,?,?,?); end;";
            cstmt = conn.prepareCall(sql);
            cstmt.registerOutParameter(1, OracleTypes.NUMBER);
            cstmt.setBinaryStream(2, new ByteArrayInputStream(doc.getAnexo()), doc.getAnexo().length);
            cstmt.setString(3, doc.getNomeArquivo());
            cstmt.setString(4, doc.getCodigoProposta());
            cstmt.setString(5, doc.getCodigoCliente());
            cstmt.setString(6, doc.getCodigoTipoTermo());
            cstmt.setString(7, doc.getChaveCertisign());
            cstmt.setString(8, doc.getIdCertisign());
            cstmt.setString(9, codigoAgenteAtz);
            cstmt.setString(10, doc.getCodigoStatusTermo());
            cstmt.execute();

            if (!(doc.getCodigoArquivo().length() > 0)) {
                doc.setCodigoArquivo(cstmt.getString(1));
            }
            
            conn.commit(); // grava os dados básicos no banco de dados
        }  catch (Exception e) {
        	conn.rollback();
            e.printStackTrace();
            throw new BadRequestException("Erro ao editar MASTER.PC_STEC_PROPOSTA.SP_MANTEM_TERMO_PROVISORIO!");
        } finally {
            try {
            	cstmt.close();
                conn.close();                
                
            } catch (SQLException e) {
                e.printStackTrace();
                throw new BadRequestException("Erro ao editar MASTER.PC_STEC_PROPOSTA.SP_MANTEM_TERMO_PROVISORIO!");
            }
        }

        return doc;
    }
    
}
