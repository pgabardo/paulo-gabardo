package br.com.sebrae.pr.sebraetec.api.util;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.base.Preconditions;

import br.com.sebrae.pr.core.util.exceptions.ExcecaoDeInfraEstrutura;

@Component
public class ConnectionFactory {
    private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class);

    @Autowired
    private DataSource defaultDataSource;

    private ConnectionFactory() {

    }

    /**
     * Obtém a conexao do datasource gerenciado pelo JBOSS e mapeado com annotation
     * EJB. <br>
     * <b>Obs: Nao alterar o autocommit da connection, pois as transacoes
     * gerenciadas pelo container.</b>
     *
     * @return Conexao
     */
    public Connection getConnection() {
        LOGGER.debug("Obtendo connection...");
           
        try {
            final Connection conn = defaultDataSource.getConnection();
            Preconditions.checkNotNull(conn, "Connection nao pode ser obtida!");
            LOGGER.debug("Connection obtida...");
            return conn;
        } catch (SQLException e) {
            LOGGER.fatal("Erro obtendo connection!", e);
            throw new ExcecaoDeInfraEstrutura(e);
        }
    }
    
}
