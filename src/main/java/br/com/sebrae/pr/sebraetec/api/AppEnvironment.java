package br.com.sebrae.pr.sebraetec.api;

import br.com.sebrae.pr.sebraetec.api.util.Constantes;
import br.com.sebrae.pr.sas.api.util.Ambiente;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

@Component
public class AppEnvironment {
    protected static final Log LOG = LogFactory.getLog(AppEnvironment.class);

    //Variáveis de ambiente
    private static final String ENV_AMBIENTE_SEBRAE = "DESENVOLVIMENTO";
    private static final String ENV_ALLOWED_ORIGINS = "variavel de ambiente";//TODO-RENAME

    private static final String ENV_DB_DRIVER = "oracle.jdbc.driver.OracleDriver";//TODO-RENAME
    private static final String ENV_DB_USERNAME = "SEBRAETEC_CON";//TODO-RENAME
    private static final String ENV_DB_PASSWORD = "SEBRAETEC";//TODO-RENAME
    private static final String ENV_DB_URL = "jdbc:oracle:thin:@10.19.4.26:1521:cbad";//TODO-RENAME

    // AS CONFIGURAÇÕES ABAIXO SÃO SOBRESCRITAS QUANDO HOUVEREM VARIÁVEIS DE AMBIENTE
    // Configurações da conexão com o banco de dados
    private static final String DEFAULT_DB_DRIVER = "oracle.jdbc.driver.OracleDriver";
    private static final String DEFAULT_DB_USERNAME = "SEBRAETEC_CON";
    private static final String DEFAULT_DB_PASSWORD = "SEBRAETEC";
    private static final String DEFAULT_DB_URL = "jdbc:oracle:thin:@10.19.4.26:1521:cbad";

    //Configurações de ambiente
    public static final String DEFAULT_CORS_ALLOWED_ORIGINS = "http://localhost:4200,https://area-logada-ng.dev.fswise.com.br,*";
    public static final String DEFAULT_SEBRAE_ENV = "DESENVOLVIMENTO";

    @Autowired
    private Environment env;

    @Bean
    public List<String> getAllowedOrigins() {
        return Arrays.asList(getCorsAllowedOrigins().split(","));
    }

    @PostConstruct
    void validarVariaveis() {
        if (StringUtils.isBlank(getStringEnvSebrae())) {
            throw new ExceptionInInitializerError("Variável de ambiente não encontrada: " + ENV_AMBIENTE_SEBRAE);
        } else {
            try {
                Ambiente.valueOf(getStringEnvSebrae()).toString();
            } catch (Exception e) {
                throw new ExceptionInInitializerError("A variável de ambiente " + ENV_AMBIENTE_SEBRAE + ", deve ter " +
                        "um dos seguintes valores: DESENVOLVIMENTO, HOMOLOGACAO ou PRODUCAO. Esta variável define qual " +
                        "SAS-API deve ser utilizado para se autenticar e para obter os endereços de outras APIs");
            }
        }
        if (StringUtils.isBlank(getCorsAllowedOrigins())) {
            throw new ExceptionInInitializerError("Variável de ambiente não encontrada: " + ENV_ALLOWED_ORIGINS);
        }
        if (StringUtils.isBlank(getDbDriverClass())) {
            throw new ExceptionInInitializerError("Variável de ambiente não encontrada: " + ENV_DB_DRIVER);
        }
        if (StringUtils.isBlank(getDbUsername())) {
            throw new ExceptionInInitializerError("Variável de ambiente não encontrada: " + ENV_DB_USERNAME);
        }
        if (StringUtils.isBlank(getDbPassword())) {
            throw new ExceptionInInitializerError("Variável de ambiente não encontrada: " + ENV_DB_PASSWORD);
        }
        if (StringUtils.isBlank(getDbUrl())) {
            throw new ExceptionInInitializerError("Variável de ambiente não encontrada: " + ENV_DB_URL);
        }

        LOG.warn("===================================================================================================");
        LOG.warn("CONFIGURACOES DE AMBIENTE =========================================================================");
        LOG.warn("===================================================================================================");
        LOG.warn("APP_KEY: " + Constantes.APP_KEY);
        LOG.warn(ENV_AMBIENTE_SEBRAE + ": " + getStringEnvSebrae());
        LOG.warn(ENV_ALLOWED_ORIGINS + ": " + getCorsAllowedOrigins());
        LOG.warn(ENV_DB_DRIVER + ": " + getDbDriverClass());
        LOG.warn(ENV_DB_USERNAME + ": " + getDbUsername());
        LOG.warn(ENV_DB_URL + ": " + getDbUrl());
        LOG.warn("===================================================================================================");
        LOG.warn("===================================================================================================");

    }

    public String getCorsAllowedOrigins() {
        return env.getProperty(ENV_ALLOWED_ORIGINS, DEFAULT_CORS_ALLOWED_ORIGINS);
    }

    String getStringEnvSebrae() {
        return env.getProperty(ENV_AMBIENTE_SEBRAE, DEFAULT_SEBRAE_ENV);
    }

    public Ambiente getAmbienteSebrae() {
        return Ambiente.valueOf(getStringEnvSebrae());
    }

    public String getDbDriverClass() {
        return env.getProperty(ENV_DB_DRIVER, DEFAULT_DB_DRIVER);
    }

    public String getDbUsername() {
        return env.getProperty(ENV_DB_USERNAME, DEFAULT_DB_USERNAME);
    }

    public String getDbPassword() {
        return env.getProperty(ENV_DB_PASSWORD, DEFAULT_DB_PASSWORD);
    }

    public String getDbUrl() {
        return env.getProperty(ENV_DB_URL, DEFAULT_DB_URL);
    }

}
