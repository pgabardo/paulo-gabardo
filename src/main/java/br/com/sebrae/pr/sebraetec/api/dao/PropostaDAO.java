package br.com.sebrae.pr.sebraetec.api.dao;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.sebrae.pr.sebraetec.api.dto.AtividadePropostaDTO;
import br.com.sebrae.pr.sebraetec.api.dto.PropostaDTO;

@Service
public interface PropostaDAO {

	public Long pesquisaCodAgenteByCPF(final Long numCPF);
	
	public List<PropostaDTO> pesquisarPropostas(Long codCLiente);
	
	public List<AtividadePropostaDTO> pesquisarAtividadesProposta(Long codProposta);
	
	public void recusarProposta(Long codProposta, Long codCliente, String justificativa);
	
	public void aprovarProposta(Long codProposta, Long codCliente);
	
	public void manterHitoricoProposta(Long codProposta, Long codCliente, String acao);
	
	public void recusarPlanoTrabalho(Long codProposta, Long codCliente);
	
    public void aprovarPlanoTrabalho(Long codProposta, Long codCliente);
    
    public void comentarProposta(Long codProposta, Long codCliente, String comentario);
}
