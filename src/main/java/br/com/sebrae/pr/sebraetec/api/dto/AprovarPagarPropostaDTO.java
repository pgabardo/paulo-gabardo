package br.com.sebrae.pr.sebraetec.api.dto;

import java.io.Serializable;

public class AprovarPagarPropostaDTO implements Serializable {

	private static final long serialVersionUID = -5726398863199195674L;
	
	private Long codCliente;    
    private Long codProposta;
    private boolean finalizacao;
    
    public AprovarPagarPropostaDTO() {
    }

	public Long getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(Long codCliente) {
		this.codCliente = codCliente;
	}

	public Long getCodProposta() {
		return codProposta;
	}

	public void setCodProposta(Long codProposta) {
		this.codProposta = codProposta;
	}

	public boolean isFinalizacao() {
		return finalizacao;
	}

	public void setFinalizacao(boolean finalizacao) {
		this.finalizacao = finalizacao;
	}
		
}
