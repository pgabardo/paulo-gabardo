package br.com.sebrae.pr.sebraetec.api.service.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.querydsl.core.BooleanBuilder;

import br.com.sebrae.pr.sebraetec.api.dao.PropostaDAO;
import br.com.sebrae.pr.sebraetec.api.dto.AtividadePropostaDTO;
import br.com.sebrae.pr.sebraetec.api.dto.PropostaDTO;
import br.com.sebrae.pr.sebraetec.api.entity.PropostaEntity;
import br.com.sebrae.pr.sebraetec.api.entity.QPropostaEntity;
import br.com.sebrae.pr.sebraetec.api.repository.PropostaRepository;
import br.com.sebrae.pr.sebraetec.api.service.PropostaService;
import br.com.wise.boot.sebrae.dto.filtro.FiltroBaseLong;
import br.com.wise.boot.sebrae.service.SebraeAbstractBaseCrudService;
import br.com.wise.boot.starter.repository.BaseCrudRepository;
import br.com.wise.common.exception.EmptyResponseException;

@Service
class PropostaServiceImpl extends SebraeAbstractBaseCrudService<Long, PropostaEntity, PropostaDTO> implements PropostaService {

	private static final long serialVersionUID = 5454156270957814521L;

	@Autowired
    private Environment env;

    @Autowired
    @Qualifier("defaultEntityManager")
    private EntityManager em;

    @Autowired
    private PropostaRepository propostaRepository;
    
    @Autowired
    private PropostaDAO propostaDAO;

    @Override
    protected BaseCrudRepository<Long, PropostaEntity> getRepository() {
        return propostaRepository;
    }

    @Override
    public String getCodUsuarioLogado() {
        return "";
    }

    @Override
    protected PropostaEntity dtoToPersistente(PropostaDTO dto) {
        PropostaEntity ret = new PropostaEntity();
        return ret;
    }

    @Override
    protected PropostaDTO persistenteToDto(PropostaEntity entity) {
        return new PropostaDTO();
    }

    @Override
    public Page<PropostaDTO> pesquisaPaginada(final FiltroBaseLong filtro) {

        final BooleanBuilder builder = new BooleanBuilder();
        builder.and(QPropostaEntity.propostaEntity.codEmpresa.eq(getCodEmpresaLogada()));
        builder.and(QPropostaEntity.propostaEntity.ativo.isTrue());

        if (filtro.getCod() != null) {
            builder.and(QPropostaEntity.propostaEntity.cod.eq(filtro.getCod()));
        }
        
        final Page<PropostaDTO> ret = propostaRepository.findAll(builder, filtro.montaPaginacao())
                .map(this::persistenteToDto);

        if (ret.getTotalElements() == 0) {
            throw new EmptyResponseException();
        }
        return ret;

    }

	@Override
	public List<PropostaDTO> pesquisarPorCodigoCliente(Long codCliente) {

		List<PropostaDTO> listaRetornada = propostaDAO.pesquisarPropostas(codCliente);
		
		if(listaRetornada != null && listaRetornada.size() > 0) {
			for (PropostaDTO propostaDTO : listaRetornada) {
				propostaDTO.setListaAtividade(pesquisarAtividadesProposta(propostaDTO.getCodProposta()));				
			}
		}
		
		
		return listaRetornada;
		
	}
	
	@Override
	public List<AtividadePropostaDTO> pesquisarAtividadesProposta(Long codProposta) {
		return propostaDAO.pesquisarAtividadesProposta(codProposta);
	}
	
	@Override
    public void recusarProposta(Long codProposta, Long codCliente, String justificativa) {
		propostaDAO.recusarProposta(codProposta, codCliente, justificativa);
		propostaDAO.comentarProposta(codProposta, codCliente, "[PROPOSTA-RECUSADA] "+justificativa);
		propostaDAO.manterHitoricoProposta(codProposta, codCliente, "Proposta recusada pelo cliente via area logada:" + justificativa);
    }
    
    @Override
    public void aprovarProposta(Long codProposta, Long codCliente) {
    	propostaDAO.aprovarProposta(codProposta, codCliente);
    	propostaDAO.comentarProposta(codProposta, codCliente, "[PROPOSTA-APROVADA] Proposta aprovada pelo cliente via area logada");
    	propostaDAO.manterHitoricoProposta(codProposta, codCliente, "Proposta aprovada pelo cliente via area logada!");
    }
    
    @Override
    public void aprovarPlanoTrabalho(Long codProposta, Long codCliente, boolean finalizacao) {
    	String tag = "[APROVADO-PLANO-TRABALHO-PARCIAL-CLIENTE] - cliente aprovou plano de trabalho parcial via area logada";
    	if(finalizacao) {
    		tag = "[APROVADO-PLANO-TRABALHO-FINALIZACAO-CLIENTE] - cliente aprovou FINALIZACAO de plano de trabalho via area logada";
    	}
    	propostaDAO.comentarProposta(codProposta, codCliente, tag);
    	propostaDAO.manterHitoricoProposta(codProposta, codCliente, tag);
    }
    
    @Override
    public void recusarPlanoTrabalho(Long codProposta, Long codCliente, String comentario) {
    	propostaDAO.recusarPlanoTrabalho(codProposta, codCliente);
    	propostaDAO.comentarProposta(codProposta, codCliente, "[RECUSADO-CLIENTE-PLANO-TRABALHO] " + comentario);
    	propostaDAO.manterHitoricoProposta(codProposta, codCliente, "Plano de trabalho recusado pelo cliente via area logada!");
    }
    

}
