package br.com.sebrae.pr.sebraetec.api.dto;

import java.io.Serializable;

public class EnvioAssinaturaDTO implements Serializable {

	private static final long serialVersionUID = -4626618244162463385L;
	
	private String auth;    
    private RelatorioVO relatorio;
    private ArquivoPropostaStecVO arquivo;
    
    public EnvioAssinaturaDTO() {
    }

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public RelatorioVO getRelatorio() {
		return relatorio;
	}

	public void setRelatorio(RelatorioVO relatorio) {
		this.relatorio = relatorio;
	}

	public ArquivoPropostaStecVO getArquivo() {
		return arquivo;
	}

	public void setArquivo(ArquivoPropostaStecVO arquivo) {
		this.arquivo = arquivo;
	}
		
}
