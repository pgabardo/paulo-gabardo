package br.com.sebrae.pr.sebraetec.api.util;

import br.com.wise.boot.sebrae.dto.AccessToken;
import br.com.wise.boot.sebrae.dto.CustomAccessTokenDTO;
import br.com.wise.boot.starter.util.reqlog.CustomRequestLoggingConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class CustomRequestLoggingFilterConfigImpl implements CustomRequestLoggingConfiguration {
    protected static final Log LOG = LogFactory.getLog(CustomRequestLoggingFilterConfigImpl.class);

    @Override
    public boolean deveLogar(final HttpServletRequest request) {
        return ((!"/public/kubernetes-liveness-probe".equals(request.getRequestURI())))
                && (!"/public/kubernetes-readiness-probe".equals(request.getRequestURI()));
    }

    @Override
    public void log(final String message, final long tempoTotal) {
        String timeLog = "";
        if (tempoTotal > 1000) {
            timeLog = ((double) tempoTotal / 1000) + " seconds";

        } else {
            timeLog = tempoTotal + " milliseconds";
        }
        LOG.info(message.replace("{time}", timeLog));
    }

    @Override
    public String criarMensagem(final HttpServletRequest request, final String message) {
        return request.getMethod() + " - {time} - (Usuário: " + getNomeUsuarioLogado() + ")" + " - " + message;
    }

    private String getNomeUsuarioLogado() {
        final CustomAccessTokenDTO at = AccessToken.get().orElse(null);
        if (at != null && at.getUsuario() != null && StringUtils.isNotBlank(at.getUsuario().getNome())) {
            return at.getUsuario().getNome();
        }
        return "Não logado";
    }

}
