package br.com.sebrae.pr.sebraetec.api.util.k8s;

import br.com.wise.boot.starter.rest.kubernetes.KubernetesProbeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.math.BigDecimal;

@Service
class KubernetesProbeServiceImpl implements KubernetesProbeService {

    @Autowired
    @Qualifier("defaultEntityManager")
    private EntityManager em;


    @Override
    public boolean applicationReadinessChecker() {
        return applicationCheck();
    }

    @Override
    public boolean applicationLivenessChecker() {
        return applicationCheck();
    }

    boolean applicationCheck() {
        try {
            final BigDecimal um = (BigDecimal) em.createNativeQuery("select 1 from dual").getSingleResult();
            return um.equals(BigDecimal.ONE);
        } catch (Exception ignored) {
        }
        return false;
    }
}
