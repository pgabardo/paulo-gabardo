package br.com.sebrae.pr.sebraetec.api;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.session.SessionManagementFilter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import br.com.sebrae.pr.sas.api.util.Ambiente;
import br.com.wise.boot.sebrae.util.AplicacaoSebraeCliente;
import br.com.wise.boot.starter.util.AplicacaoCliente;
import br.com.wise.boot.starter.util.CORSFilter;
import br.com.wise.boot.starter.util.CustomDateDeserializer;
import br.com.wise.boot.starter.util.CustomDateSerializer;
import br.com.wise.common.exception.ForbiddenError;
import br.com.wise.common.exception.base.RestException;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class},
        scanBasePackages = {"br.com.wise", "br.com.sebrae.pr"})
@EnableTransactionManagement
@EnableScheduling
@EnableAsync
public class SebraetecSebraePRApplication extends SpringBootServletInitializer {
    protected static final Log LOG = LogFactory.getLog(SebraetecSebraePRApplication.class);

    private static final String dateTimeFormat = "yyyy-MM-dd HH:mm:ss";

    @Autowired
    private AppEnvironment appEnv;

    @Autowired
    private Environment env;

    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jsonCustomizer() {
        return builder -> {
            builder.simpleDateFormat(dateTimeFormat);
            builder.serializers(new CustomDateSerializer());
            builder.deserializers(new CustomDateDeserializer());
        };
    }

    @Bean
    public AplicacaoCliente getAplicacaoCliente() {

        return new AplicacaoCliente() {

            @Override
            public String getNomeAplicacao() {
                return env.getProperty("app.name");
            }

            @Override
            public String getEnvironment() {
                return appEnv.getAmbienteSebrae().toString();
            }

            @Override
            public String getBuildTimestamp() {
                return env.getProperty("mvn.build.timestamp");
            }

            @Override
            public String getSwaggerBasePackage() {
                return "br.com.sebrae.pr";
            }

            @Override
            public RestException handleKnownException(Exception ex) {
                if (ex != null && ex instanceof AccessDeniedException) {
                    return new ForbiddenError("Acesso negado!");
                }
                return null;
            }

        };
    }

    @Bean
    public AplicacaoSebraeCliente getAplicacaoSebraeCliente() {

        return new AplicacaoSebraeCliente() {

            @Override
            public String getUrlSasApiParaDesenvolvimento() {
                return env.getProperty("url-sas-api-desenvolvimento");
            }

            @Override
            public Log getLog() {
                return LOG;
            }

            @Override
            public Ambiente getAmbiente() {
                return appEnv.getAmbienteSebrae();
            }

        };
    }

    public SebraetecSebraePRApplication() {

    }

    @Bean
    CORSFilter corsFilter() {
        return new CORSFilter();
    }

    public static void main(String[] args) {
        SpringApplication.run(SebraetecSebraePRApplication.class, args);
    }

    // interessante
    // http://www.baeldung.com/spring-boot-application-configuration

    //actuators
    // https://docs.spring.io/spring-boot/docs/2.0.x/actuator-api/html/
    // https://www.baeldung.com/spring-boot-actuators
    // custom address
    // http://localhost:8080/wise-blank-api/actuator/health


    @Configuration
    @EnableWebSecurity
    public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.addFilterBefore(corsFilter(), SessionManagementFilter.class).authorizeRequests().anyRequest().permitAll();
            http.cors().disable().csrf().disable().authorizeRequests()
                    .anyRequest().permitAll().and().httpBasic();
        }
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SebraetecSebraePRApplication.class);
    }

}
