package br.com.sebrae.pr.sebraetec.api.dao;

import java.sql.Connection;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.sebrae.pr.core.rest.dao.CoreRestBaseDao;
import br.com.sebrae.pr.sas.api.dto.AccessTokenDTO;
import br.com.sebrae.pr.sebraetec.api.util.ConnectionFactory;

public abstract class SebraetecPRBaseDao extends CoreRestBaseDao {
	
	@Autowired
	private ConnectionFactory connectionFactory;

	@Override
	public Connection getConnection() {
		return connectionFactory.getConnection();
	}

	public boolean isUserAdministrador(final AccessTokenDTO accessToken) {
		return accessToken != null && accessToken.getUsuario() != null && accessToken.getUsuario().isAdministrador();
	}

}
