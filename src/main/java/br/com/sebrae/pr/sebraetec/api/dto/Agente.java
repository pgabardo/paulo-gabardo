package br.com.sebrae.pr.sebraetec.api.dto;

public class Agente {

  private String codigoAgente;
  private String cep;  
  private String logradouro;
  private String numero;
  private String complemento;
  private String fax;
  private String homePage;
  private String nome;
  private String tel1;
  private String tel2;
  private String siglaPais;
  private String codDominAg;
  private String codDominTel1;
  private String codDominTel2;
  private String codUsuario;
  private String cel;
  private boolean indMd;
  private boolean indEmail;
  private boolean indFone;
  private boolean indFax;
  private boolean indSms;
  private String dddTel1;
  private String dddTel2;
  private String dddCel;
  private String dddFax;
  private String obs;
  private String cpf;
  private String email;
  private String email2;
  private String login;
  private String senha;
  private String sexo;
  private String codigoPessoa;
  private String token;
  private String telefone;
  private String codDominTpTel;

  public Agente() {
  }
  
  public String toString() {
    return this.nome;
  }

  public void setCodigoAgente(String codAgente) {
    this.codigoAgente = codAgente;
  }

  public String getCodigoAgente() {
    return codigoAgente;
  }

  public void setCep(String cep) {
    this.cep = cep;
  }

  public String getCep() {
    return cep;
  }

  public void setLogradouro(String logradouro) {
    this.logradouro = logradouro;
  }

  public String getLogradouro() {
    return logradouro;
  }

  public void setNumero(String numero) {
    this.numero = numero;
  }

  public String getNumero() {
    return numero;
  }

  public void setComplemento(String complemento) {
    this.complemento = complemento;
  }

  public String getComplemento() {
    return complemento;
  }

  public void setFax(String fax) {
    this.fax = fax;
  }

  public String getFax() {
    return fax;
  }

  public void setHomePage(String homePage) {
    this.homePage = homePage;
  }

  public String getHomePage() {
    return homePage;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getNome() {
    return nome;
  }

  public void setTel1(String tel1) {
    this.tel1 = tel1;
  }

  public String getTel1() {
    return tel1;
  }

  public void setTel2(String tel2) {
    this.tel2 = tel2;
  }

  public String getTel2() {
    return tel2;
  }

  public void setSiglaPais(String siglaPais) {
    this.siglaPais = siglaPais;
  }

  public String getSiglaPais() {
    return siglaPais;
  }

  public void setCodDominAg(String codDominAg) {
    this.codDominAg = codDominAg;
  }

  public String getCodDominAg() {
    return codDominAg;
  }

  public void setCodDominTel1(String codDominTel1) {
    this.codDominTel1 = codDominTel1;
  }

  public String getCodDominTel1() {
    return codDominTel1;
  }

  public void setCodDominTel2(String codDominTel2) {
    this.codDominTel2 = codDominTel2;
  }

  public String getCodDominTel2() {
    return codDominTel2;
  }

  public void setCodUsuario(String codUsuario) {
    this.codUsuario = codUsuario;
  }

  public String getCodUsuario() {
    return codUsuario;
  }

  public void setCel(String cel) {
    this.cel = cel;
  }

  public String getCel() {
    return cel;
  }

  public void setIndMd(boolean indMd) {
    this.indMd = indMd;
  }

  public boolean isIndMd() {
    return indMd;
  }

  public void setIndEmail(boolean indEmail) {
    this.indEmail = indEmail;
  }

  public boolean isIndEmail() {
    return indEmail;
  }

  public void setIndFone(boolean indFone) {
    this.indFone = indFone;
  }

  public boolean isIndFone() {
    return indFone;
  }

  public void setIndFax(boolean indFax) {
    this.indFax = indFax;
  }

  public boolean isIndFax() {
    return indFax;
  }

  public void setIndSms(boolean indSms) {
    this.indSms = indSms;
  }

  public boolean isIndSms() {
    return indSms;
  }

  public void setDddTel1(String dddTel1) {
    this.dddTel1 = dddTel1;
  }

  public String getDddTel1() {
    return dddTel1;
  }

  public void setDddTel2(String dddTel2) {
    this.dddTel2 = dddTel2;
  }

  public String getDddTel2() {
    return dddTel2;
  }

  public void setDddCel(String dddCel) {
    this.dddCel = dddCel;
  }

  public String getDddCel() {
    return dddCel;
  }

  public void setDddFax(String dddFax) {
    this.dddFax = dddFax;
  }

  public String getDddFax() {
    return dddFax;
  }

  public void setObs(String obs) {
    this.obs = obs;
  }

  public String getObs() {
    return obs;
  }

  public void setCpf(String docId) {
    this.cpf = docId;
  }

  public String getCpf() {
    return cpf;
  }

  public void setEmail(String email1) {
    this.email = email1;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail2(String email2) {
    this.email2 = email2;
  }

  public String getEmail2() {
    return email2;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getLogin() {
    return login;
  }

  public void setSenha(String senha) {
    this.senha = senha;
  }

  public String getSenha() {
    return senha;
  }

  public void setSexo(String sexo) {
    this.sexo = sexo;
  }

  public String getSexo() {
    return sexo;
  }

  public void setCodigoPessoa(String codPessoa) {
    this.codigoPessoa = codPessoa;
  }

  public String getCodigoPessoa() {
    return codigoPessoa;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getToken() {
    return token;
  }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setCodDominTpTel(String codDominTpTel) {
        this.codDominTpTel = codDominTpTel;
    }

    public String getCodDominTpTel() {
        return codDominTpTel;
    }
}
