package br.com.sebrae.pr.sebraetec.api.util;

import br.com.sebrae.pr.sebraetec.api.AppEnvironment;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.instrument.classloading.LoadTimeWeaver;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableJpaRepositories(
        basePackages = {"br.com.wise", "br.com.sebrae.pr"},
        entityManagerFactoryRef = "defaultEntityManager",
        transactionManagerRef = "defaultTransactionManager"
)
public class DefaultDbConfig {

    @Autowired
    private Environment env;

    @Autowired
    private AppEnvironment appEnv;

    @Bean
    @Primary
    public LocalContainerEntityManagerFactoryBean defaultEntityManager() {

        final LocalContainerEntityManagerFactoryBean lcemfb = new LocalContainerEntityManagerFactoryBean();
        lcemfb.setBeanName("defaultEntityManager");
        lcemfb.setPackagesToScan("br.com.wise", "br.com.sebrae.pr");
        lcemfb.setDataSource(defaultDataSource());

        final Properties props = new Properties();
        props.setProperty("hibernate.jdbc.batch_size", env.getProperty("spring.jpa.properties.hibernate.jdbc.batch_size"));
        props.setProperty("hibernate.order_inserts", "true");
        props.setProperty("hibernate.order_updates", "true");
        lcemfb.setJpaProperties(props);

        final LoadTimeWeaver loadTimeWeaver = new InstrumentationLoadTimeWeaver();
        lcemfb.setLoadTimeWeaver(loadTimeWeaver);

        final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        lcemfb.setJpaVendorAdapter(vendorAdapter);

        lcemfb.afterPropertiesSet();

        return lcemfb;
    }

    @Bean
    @Primary
    public DataSource defaultDataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
//        dataSource.setDriverClassName(appEnv.getDbDriverClass());
        dataSource.setUsername(appEnv.getDbUsername());
        dataSource.setPassword(appEnv.getDbPassword());
        dataSource.setUrl(appEnv.getDbUrl());
        return dataSource;
    }

    @Bean
    @Primary
    public PlatformTransactionManager defaultTransactionManager() {
        final EntityManagerFactory obj = defaultEntityManager().getObject();
        if (obj == null) {
            throw new ExceptionInInitializerError("EntityManagerFactory não deveria ser nulo!");
        }
        return new JpaTransactionManager(obj);
    }

}
