package br.com.sebrae.pr.sebraetec.api.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import br.com.sebrae.pr.core.util.persistence.IConverter;
import br.com.sebrae.pr.core.util.persistence.ItemResultset;
import br.com.sebrae.pr.core.util.persistence.converters.IConverterLong;
import br.com.sebrae.pr.core.util.persistence.params.InParam;
import br.com.sebrae.pr.core.util.persistence.params.ParamType;
import br.com.sebrae.pr.sebraetec.api.dao.PropostaDAO;
import br.com.sebrae.pr.sebraetec.api.dao.SebraetecPRBaseDao;
import br.com.sebrae.pr.sebraetec.api.dto.AtividadePropostaDTO;
import br.com.sebrae.pr.sebraetec.api.dto.PropostaDTO;
import br.com.wise.common.exception.BadRequestException;

@Service
public class PropostaDAOImpl extends SebraetecPRBaseDao implements PropostaDAO {

    @Override
    public Long pesquisaCodAgenteByCPF(final Long cpf) {

        if (cpf.longValue() == 0L) {
            return null;
        }

        final StringBuilder sql = new StringBuilder();
        sql.append("SELECT t15.a0015_cod_agente     ");
        sql.append("  FROM master.tb0015_agente t15     ");
        sql.append(" WHERE t15.a0015_doc_id = ?     ");
        sql.append("   AND t15.a0015_ind_ativo = 1     ");
        sql.append("   AND ROWNUM <= 1     ");
        sql.append(" ORDER BY t15.a0015_cod_agente DESC");

        final List<InParam> params = new ArrayList<InParam>();
        params.add(new InParam(1, cpf.longValue(), ParamType.NUMERIC));

        final IConverterLong codAgenteOut = new IConverterLong("a0015_cod_agente");
        final List<Long> id = executarQuery(sql, params, codAgenteOut);

        if (id != null && id.iterator().hasNext()) {
            return id.iterator().next();
        }

        return null;

    }
    
    @Override
    public List<PropostaDTO> pesquisarPropostas(Long codCLiente) {
    	
    	final StringBuilder sql = new StringBuilder();
        sql.append("select t931.a0015_cod_agente_cli COD_CLI, "
        		+ " master.fn_busca_nome('agente', t931.a0015_cod_agente_cli) NOME_CLIENTE, "
                + " t930.A0930_COD_PROPOSTA, "
                + " t930.A0930_COD_PROPOSTA_ORIGEM, "
                + " (select  max(level) -1 "
                + " from master.tb0930_st2_proposta t930a "
                + "    where t930a.a0930_cod_proposta = t930.a0930_cod_proposta "
                + "     connect by prior t930a.a0930_cod_proposta = t930a.a0930_cod_proposta_origem) SEQ_CONTINUIDADE, "
                + " t930.A0939_COD_STATUS, "
                + " t939.A0939_LABEL_STATUS A0939_LABEL_STATUS, "
                + " t930.A0926_COD_CATEGORIA, "
                + " master.pc_stec_cadastro.FN_GET_CATEGORIA(t930.A0926_COD_CATEGORIA) A0926_DSC_CATEGORIA , "
                + " (select nvl(t.a0926_ind_inovacao,0) "
                + " from   master.tb0926_st2_categoria t "
                + " where  t.a0926_cod_categoria = t930.A0926_COD_CATEGORIA) A0926_IND_INOVACAO,  "
                + " t930.A0927_COD_TIPO_DEMANDA, "
                + " master.pc_stec_cadastro.FN_GET_DEMANDA(t930.A0927_COD_TIPO_DEMANDA) A0927_DSC_TIPO_DEMANDA, "
                + " master.pc_stec_proposta.FN_GET_TIPO_CONTAB_DEMANDA(t930.A0927_COD_TIPO_DEMANDA) a0927_tipo_contab, "
                + " t930.A0923_COD_SERVICO, "
                + " master.pc_stec_proposta.FN_GET_DSC_PROP_SERVICO(t930.A0930_COD_PROPOSTA) DSC_PROP_SERVICO, "
                + " t930.A0930_COD_CCUSTO, "
                + " t930.A0930_CC_DESCRICAO, "
                + " t930.A0930_CC_ANO, "
                + " t930.A0930_CC_SALDO_DISP, "
                + " t930.A0930_CC_SALDO_COMPROMETIDO, "
                + " t930.A0930_CC_SALDO_NOV_PROJ, "
                + " t930.A0930_VLR_PROPOSTA, "
                + " 'R$ '||MASTER.PC_STEC.FN_FM_CURRENT_BR(t930.A0930_VLR_PROPOSTA)  A0930_VLR_PROPOSTA_FM, "
                + " t930.A0930_VLR_CLIENTE, "
                + " 'R$ '||MASTER.PC_STEC.FN_FM_CURRENT_BR(t930.A0930_VLR_CLIENTE)  A0930_VLR_CLIENTE_FM, "
                + " t930.A0930_VLR_CONTRAPARTIDA, "
                + " 'R$ '||MASTER.PC_STEC.FN_FM_CURRENT_BR(t930.A0930_VLR_CONTRAPARTIDA)  A0930_VLR_CONTRAPARTIDA_FM, "
                + " t930.A0930_NECESSIDADE, "
                + " t930.A0930_NR_CLIENTES_PREVISTOS, "
                + " t930.A0015_COD_AGENTE_SOLIC, "
                + " master.fn_busca_nome('agente', t930.A0015_COD_AGENTE_SOLIC) A0015_NM_AGENTE_SOLIC, "
                + " (select t110.a0110_tel_sebrae "
                + " from   master.tb0110_vinculo_agente t110 "
                + "  where  t110.a0110_ind_ativo = 1 "
                + " and    t110.a0015_cod_agente_vinc = t930.A0015_COD_AGENTE_SOLIC "
                + " and rownum <=1) TELEFONE_SOLIC, "
                + " master.fn_busca_email_agente(t930.A0015_COD_AGENTE_SOLIC) A0015_EMAIL_SOLIC, "
                + " t930.A0015_COD_AGENTE_RESP, "
                + " master.fn_busca_nome('agente', t930.A0015_COD_AGENTE_RESP) A0015_NM_AGENTE_RESP, "
                + " (select t110.a0110_tel_sebrae "
                + " from   master.tb0110_vinculo_agente t110 "
                + " where  t110.a0110_ind_ativo = 1 "
                + "  and    t110.a0015_cod_agente_vinc = t930.A0015_COD_AGENTE_RESP "
                + "  and rownum <=1) TELEFONE_RESP, "
                + "  master.fn_busca_email_agente(t930.A0015_COD_AGENTE_RESP) A0015_EMAIL_RESP, "
                + " t930.A0930_DSC_SOLUCAO, "
                + " t930.A0930_DSC_RESULTADO, "
                + " t930.A0930_JUSTIFICATIVA, "
                + " t930.A0930_CANCELAMENTO, "
                + " t930.A0015_COD_AGENTE_GER, "
                + " master.fn_busca_nome('agente', t930.a0015_cod_agente_ger) A0015_NM_AGENTE_GER, "
                + " master.pc_stec_proposta.FN_GET_CONFIG('COMITE_INOVACAO') A0015_COD_AGENTES_COMITE, "
                + " master.pc_stec_proposta.FN_GET_CONFIG('APROV_UIC') A0015_COD_AGENTES_UIC, "
                + " t930.A0930_DT_VAL_PLANO_RESP, "
                + " to_char(t930.a0930_dt_val_plano_resp, 'DD/MM/YYYY') A0930_DT_VAL_PLANO_RESP_FM, "
                + " t930.A0930_DT_VAL_PLANO_GERENTE, "
                + " to_char(t930.a0930_dt_val_plano_gerente, 'DD/MM/YYYY') A0930_DT_VAL_PLANO_GERENTE_FM, "
                + " t930.A0930_DT_VAL_PLANO_COMITE, "
                + " to_char(t930.a0930_dt_val_plano_comite, 'DD/MM/YYYY') A0930_DT_VAL_PLANO_COMITE_FM, "

                + " t930.A0011_COD_UNID, "
                + " (select  t11.a0011_dsc_unid "
                + " from    master.tb0011_unidade t11 "
                + " where   t11.a0011_cod_unid = t930.A0011_COD_UNID) A0011_DSC_UNID, "

                + " t930.A0930_COD_AGENTE_PATROCINADOR, "
                + " master.fn_busca_nome('agente', t930.a0930_cod_agente_patrocinador) A0015_NM_AGENTE_PATROCINADOR, "
                + " t930.A0930_CC_PATROCINADOR, "
                + " t930.A0930_CC_ANO_PATROCINADOR, "
                + " t930.A0930_DSC_CC_PATROCINADOR, "
                + " T930.A0930_CC_PATROCINADOR_RECEITA, "
                + " T930.A0930_DSC_CC_PATROCINADOR_REC, "
                + " T930.A0930_CC_ANO_PATROCINADOR_REC, "

                + " t930.A0930_COD_AGENTE_PARCEIRO, "
                + " master.fn_busca_nome('agente', t930.a0930_cod_agente_parceiro) A0015_NM_AGENTE_PARCEIRO, "
                + " t930.A0930_CC_ANO_RECEITA, "
                + " t930.A0930_CC_RECEITA_CLIENTE, "
                + " t930.A0930_DSC_RECEITA_CLIENTE, "
                + " t930.A0930_CC_ANO_DESPESA, "
                + " t930.A0930_CC_DESPESA, "
                + " t930.A0930_DSC_DESPESA, "
                + " t930.A0930_CC_DESP_SALDO_DISP, "
                + " t930.A0930_CC_DESP_SALDO_COMPR, "
                + " t930.A0930_CC_DESP_SALDO_NOV_PROJ, "

                + " t930.A0930_CC_ANO_OUTRAS, "
                + " t930.A0930_CC_OUTRAS_DESPESA, "
                + " t930.A0930_DSC_OUTRAS_DESPESA, "

                + " t930.A0930_CC_SALDO, "
                + " t930.A0930_CC_ANO_SALDO, "
                + " t930.A0930_CC_DSC_SALDO, "

                + " t930.a0930_vlr_sebrae  A0930_VLR_SEBRAE, "
                + " 'R$ '||MASTER.PC_STEC.FN_FM_CURRENT_BR(t930.a0930_vlr_sebrae)  A0930_VLR_SEBRAE_FM, "
                + " MASTER.PC_STEC.FN_FM_CURRENT_BR(t930.a0930_vlr_sebrae)  A0930_VLR_SEBRAE_FME, "

                + " t930.a0930_vlr_sebrae_na  A0930_VLR_SEBRAE_NA, "
                + " 'R$ '||MASTER.PC_STEC.FN_FM_CURRENT_BR(t930.a0930_vlr_sebrae_na)  A0930_VLR_SEBRAE_NA_FM, "
                + " MASTER.PC_STEC.FN_FM_CURRENT_BR(t930.a0930_vlr_sebrae_na)  A0930_VLR_SEBRAE_NA_FME, "

                + " t930.a0930_valor_patrocinado  A0930_VLR_PATROCINADO, "
                + " 'R$ '||MASTER.PC_STEC.FN_FM_CURRENT_BR(t930.a0930_valor_patrocinado)  A0930_VLR_PATROCINADO_FM, "
                + " MASTER.PC_STEC.FN_FM_CURRENT_BR(t930.a0930_valor_patrocinado) A0930_VLR_PATROCINADO_FME, "

                + " t930.a0930_valor_parceiro  A0930_VLR_PARCEIRO, "
                + " 'R$ '||MASTER.PC_STEC.FN_FM_CURRENT_BR(t930.a0930_valor_parceiro)  A0930_VLR_PARCEIRO_FM, "
                + " MASTER.PC_STEC.FN_FM_CURRENT_BR(t930.a0930_valor_parceiro)  A0930_VLR_PARCEIRO_FME, "
                + " MASTER.PC_STEC.FN_FM_CURRENT_BR(t930.a0930_vlr_fixo)  A0930_VLR_FIXO_FME, "
                + " t930.A0930_IND_ALT_FINANCEIRO, "
                + " t930.A0015_COD_AGENTE_INC, "
                + " master.fn_busca_nome('agente', t930.A0015_COD_AGENTE_INC) A0015_NM_AGENTE_INC, "
                + " t930.A0015_COD_AGENTE_ATZ, "
                + " master.fn_busca_nome('agente', t930.A0015_COD_AGENTE_ATZ) A0015_NM_AGENTE_ATZ, "
                + " t930.A0930_DT_INC, "
                + " to_char(t930.a0930_dt_inc, 'DD/MM/YYYY') A0930_DT_INC_FM, "
                + " t930.A0930_DT_ATZ, "
                + " to_char(t930.A0930_DT_ATZ, 'DD/MM/YYYY') A0930_DT_ATZ_FM, "
                + " t930.A0930_IND_ATIVO, "
                + " t930.A0930_COMO_CONHECE_SEBRAETEC, "
                + " t930.A0930_IND_QUEST_REACAO, "
                + " t930.A0930_IND_QUEST_EFETIVIDADE, "
                + " nvl((select 1 "
                + "        from master.tb1294_st2_prop_entidade t1294 "
                + "       where t1294.a0930_cod_proposta = t930.a0930_cod_proposta "
                + "         and t1294.a1294_ordem = 0 "
                + "         and t1294.a1294_ind_ativo = 1),0) POSSUI_ENT_EXTRA, "
                + " nvl(t930.a0930_ind_individual_coletiva,0) A0930_IND_INDIVIDUAL_COLETIVA, "
                + " nvl(t930.a0930_ind_contab_orient,0) A0930_IND_CONTAB_ORIENT, "
                + " (select count(1) from master.tb1402_certisign t1402 "
                + " inner join master.tb1299_st2_prop_termos t1299 on t1299.a0689_cod_arquivo = t1402.a0689_cod_arquivo "
                + " inner join master.tb0689_arquivo t689 on t689.a0689_cod_arquivo = t1402.a0689_cod_arquivo "
                + " where t689.a0689_ind_ativo = 1 "
                + " and   t1299.a0015_cod_agente_cli = 467 "
                + " and   t1299.a0930_cod_proposta = t930.A0930_COD_PROPOSTA) IND_PACTO_COLETIVO_ASSINADO, "
                + " nvl((select t1299.a0689_cod_arquivo from master.tb1299_st2_prop_termos t1299 "
                + "      where t1299.a1299_ind_ativo = 0 "
                + "      and   t1299.a0015_cod_agente_cli = 467 "
                + "      and   t1299.a0200_cod_domin_termo = 1451 "
                + "      and   t1299.a0930_cod_proposta = t930.A0930_COD_PROPOSTA),0) A0689_COD_ARQ_PACTO_COLETIVO, "
                + " to_char(t930.a0930_dt_fin_parcial, 'DD/MM/YYYY') A0930_DT_FIN_PARCIAL_FM, "
                + " MASTER.PC_STEC.FN_FM_CURRENT_BR(t930.a0930_perc_fin_parcial) A0930_PERC_FIN_PARCIAL, "
                + " t930.a0930_justificativa A0930_JUSTIFICATIVA_RECUSA, "
                
				+ " (select t1294.a1294_plano_necessidade "
				+ "         from master.tb0015_agente t15 "
				+ "         inner join master.tb1294_st2_prop_entidade t1294 "
				+ "              on t1294.a0015_cod_agente_cont_ent = t15.a0015_cod_agente "
				+ "         where t1294.a0930_cod_proposta = t930.a0930_cod_proposta "
				+ "         and t1294.a1294_ind_escolhido = 1) NECESSIDADE_PLANO, "
				
				+ " (select master.fn_busca_nome('agente', t1294.a0015_cod_agente_ent)  "
				+ " 	from master.tb1294_st2_prop_entidade t1294 "
                + " 		where t1294.a0930_cod_proposta   = t930.a0930_cod_proposta "
                + " 			and t1294.a1294_ind_escolhido = 1) NOME_ENTIDADE, " 
                + " (select master.fn_busca_nome('agente', t1294.a0015_cod_agente_coord_ent) " 
                + "         from master.tb1294_st2_prop_entidade t1294 "
                + "              where t1294.a0930_cod_proposta   = t930.a0930_cod_proposta "
                + "                and t1294.a1294_ind_escolhido = 1) NOME_RESP_ENTIDADE, "               
                + " (select CASE WHEN t15.a0015_ddd_tel_1 IS NULL THEN NULL " 
                + "              ELSE '(' || t15.a0015_ddd_tel_1 || ') ' || t15.a0015_tel_1 END "
                + "         from master.tb1294_st2_prop_entidade t1294 LEFT JOIN MASTER.TB0015_AGENTE t15 "
                + "         ON (t15.a0015_cod_agente = t1294.a0015_cod_agente_ent) "
                + "              where t1294.a0930_cod_proposta   = t930.a0930_cod_proposta "
                + "                and t1294.a1294_ind_escolhido = 1) FONE_RESP_ENTIDADE, "               
                + " (select t15.a0015_email  "
                + "         from master.tb1294_st2_prop_entidade t1294 " 
                + "                              LEFT JOIN MASTER.TB0015_AGENTE t15 "
                + "         ON (t15.a0015_cod_agente = t1294.a0015_cod_agente_ent) "
                + "              where t1294.a0930_cod_proposta   = t930.a0930_cod_proposta "
                + "                and t1294.a1294_ind_escolhido = 1) EMAIL_ENTIDADE,   "
                + "  master.pc_stec_proposta.FN_GET_SUBSERVICOS_PROP(t930.a0930_cod_proposta) DSC_SUBSERVICO, "
                + "  master.fn_busca_dsc_dominio(t930.a0200_cod_domin_mot_canc) MOTIVO_CANCELAMENTO, "
                + "  t1294.a0015_cod_agente_ent COD_ENTIDADE"
           + " from master.tb0930_st2_proposta    t930, "
                + " master.tb0939_st2_prop_status t939, "
                + " master.tb0931_st2_prop_cliente t931 "
          + " where t930.a0939_cod_status = t939.a0939_cod_status "
                + " and t931.a0930_cod_proposta = t930.a0930_cod_proposta "
                + " and t931.a0931_ind_ativo = 1 "
                + " and t930.A0930_IND_ATIVO = 1 "
                + " and t930.a0930_ind_individual_coletiva = 0 "
                + " and t931.a0015_cod_agente_cli = ? " );

        final List<InParam> params = new ArrayList<InParam>();
        params.add(new InParam(1, codCLiente, ParamType.NUMERIC));

        final IConverter<PropostaDTO> strConverter = new IConverter<PropostaDTO>() {

            @Override
            public PropostaDTO convert(ItemResultset reg) {
            	PropostaDTO propostaDTO = new PropostaDTO();
            propostaDTO.setCodCli(reg.getLong("COD_CLI"));                                                                        	
			propostaDTO.setNomeCliente(reg.getString("NOME_CLIENTE"));                                                            	
			propostaDTO.setCodProposta(reg.getLong("A0930_COD_PROPOSTA"));                                                        	
			propostaDTO.setCodPropostaOrigem(reg.getLong("A0930_COD_PROPOSTA_ORIGEM"));                                           	
			propostaDTO.setSeqContinuidade(reg.getLong("SEQ_CONTINUIDADE"));                                                      	
			propostaDTO.setCodStatus(reg.getLong("A0939_COD_STATUS"));                                                            	
			propostaDTO.setLabelStatus(reg.getString("A0939_LABEL_STATUS"));                                                      	
			propostaDTO.setCodCategoria(reg.getLong("A0926_COD_CATEGORIA"));                                                      	
			propostaDTO.setDscCategoria(reg.getString("A0926_DSC_CATEGORIA"));                                                    	
			propostaDTO.setIndInovacao(reg.getBoolean("A0926_IND_INOVACAO", false));                                              	
			propostaDTO.setCodTipoDemanda(reg.getLong("A0927_COD_TIPO_DEMANDA"));                                                 	
			propostaDTO.setDscTipoDemanda(reg.getString("A0927_DSC_TIPO_DEMANDA"));                                               	
			propostaDTO.setTipoContab(reg.getLong("A0927_TIPO_CONTAB"));                                                          	
			propostaDTO.setCodServico(reg.getLong("A0923_COD_SERVICO"));                                                          	
			propostaDTO.setDscPropServico(reg.getString("DSC_PROP_SERVICO"));                                                     	
			propostaDTO.setCodCcusto(reg.getLong("A0930_COD_CCUSTO"));                                                            	
			propostaDTO.setCcDescricao(reg.getString("A0930_CC_DESCRICAO"));                                                      	
			propostaDTO.setCcAno(reg.getString("A0930_CC_ANO"));                                                                  	
			propostaDTO.setCcSaldoDisp(reg.getString("A0930_CC_SALDO_DISP"));                                                     	
			propostaDTO.setCcSaldoComprometido(reg.getString("A0930_CC_SALDO_COMPROMETIDO"));                                     	
			propostaDTO.setCcSaldoNovProj(reg.getString("A0930_CC_SALDO_NOV_PROJ"));                                              	
			propostaDTO.setVlrProposta(reg.getLong("A0930_VLR_PROPOSTA"));    
			propostaDTO.setVlrPropostaFm(reg.getString("A0930_VLR_PROPOSTA_FM"));	
			propostaDTO.setVlrCliente(reg.getLong("A0930_VLR_CLIENTE"));
			propostaDTO.setVlrClienteFm(reg.getString("A0930_VLR_CLIENTE_FM"));
			propostaDTO.setVlrContrapartida(reg.getLong("A0930_VLR_CONTRAPARTIDA"));
			propostaDTO.setVlrContrapartidaFm(reg.getString("A0930_VLR_CONTRAPARTIDA_FM"));
			propostaDTO.setNecessidade(reg.getString("A0930_NECESSIDADE"));                                                       	
			propostaDTO.setNrClientesPrevistos(reg.getLong("A0930_NR_CLIENTES_PREVISTOS"));                                       	
			propostaDTO.setCodAgenteSolic(reg.getLong("A0015_COD_AGENTE_SOLIC"));                                                 	
			propostaDTO.setNmAgenteSolic(reg.getString("A0015_NM_AGENTE_SOLIC"));                                                 	
			propostaDTO.setTelefoneSolic(reg.getString("TELEFONE_SOLIC"));                                                        	
			propostaDTO.setEmailSolic(reg.getString("A0015_EMAIL_SOLIC"));                                                        	
			propostaDTO.setCodAgenteResp(reg.getLong("A0015_COD_AGENTE_RESP"));                                                   	
			propostaDTO.setNmAgenteResp(reg.getString("A0015_NM_AGENTE_RESP"));                                                   	
			propostaDTO.setTelefoneResp(reg.getString("TELEFONE_RESP"));                                                          	
			propostaDTO.setEmailResp(reg.getString("A0015_EMAIL_RESP"));                                                          	
			propostaDTO.setDscSolucao(reg.getString("A0930_DSC_SOLUCAO"));                                                        	
			propostaDTO.setDscResultado(reg.getString("A0930_DSC_RESULTADO"));                                                    	
			propostaDTO.setJustificativa(reg.getString("A0930_JUSTIFICATIVA"));                                                   	
			propostaDTO.setCancelamento(reg.getString("A0930_CANCELAMENTO"));                                                     	
			propostaDTO.setCodAgenteGer(reg.getLong("A0015_COD_AGENTE_GER"));                                                     	
			propostaDTO.setNmAgenteGer(reg.getString("A0015_NM_AGENTE_GER"));                                                     	
			propostaDTO.setTelefoneGer(reg.getString("TELEFONE_GER"));                                                            	
			propostaDTO.setCodAgentesComite(reg.getLong("A0015_COD_AGENTES_COMITE"));                                             	
			propostaDTO.setCodAgentesUic(reg.getLong("A0015_COD_AGENTES_UIC"));                                                   	
			propostaDTO.setDtValPlanoResp(reg.getDate("A0930_DT_VAL_PLANO_RESP"));                                                	
			propostaDTO.setDtValPlanoRespFm(reg.getDate("A0930_DT_VAL_PLANO_RESP_FM"));                                           	
			propostaDTO.setDtValPlanoGerente(reg.getDate("A0930_DT_VAL_PLANO_GERENTE"));                                          	
			propostaDTO.setDtValPlanoGerenteFm(reg.getDate("A0930_DT_VAL_PLANO_GERENTE_FM"));                                     	
			propostaDTO.setDtValPlanoComite(reg.getDate("A0930_DT_VAL_PLANO_COMITE"));                                            	
			propostaDTO.setDtValPlanoComiteFm(reg.getDate("A0930_DT_VAL_PLANO_COMITE_FM"));                                       	
			propostaDTO.setCodUnid(reg.getLong("A0011_COD_UNID"));                                                                	
			propostaDTO.setDscUnid(reg.getString("A0011_DSC_UNID"));                                                              	
			propostaDTO.setCodAgentePatrocinador(reg.getLong("A0930_COD_AGENTE_PATROCINADOR"));                                   	
			propostaDTO.setNmAgentePatrocinador(reg.getString("A0015_NM_AGENTE_PATROCINADOR"));                                   	
			propostaDTO.setCcPatrocinador(reg.getString("A0930_CC_PATROCINADOR"));                                                	
			propostaDTO.setCcAnoPatrocinador(reg.getString("A0930_CC_ANO_PATROCINADOR"));                                         	
			propostaDTO.setDscCcPatrocinador(reg.getString("A0930_DSC_CC_PATROCINADOR"));                                         	
			propostaDTO.setCcPatrocinadorReceita(reg.getString("A0930_CC_PATROCINADOR_RECEITA"));                                 	
			propostaDTO.setDscCcPatrocinadorRec(reg.getString("A0930_DSC_CC_PATROCINADOR_REC"));                                  	
			propostaDTO.setCcAnoPatrocinadorRec(reg.getString("A0930_CC_ANO_PATROCINADOR_REC"));                                  	
			propostaDTO.setCodAgenteParceiro(reg.getString("A0930_COD_AGENTE_PARCEIRO"));                                         	
			propostaDTO.setNmAgenteParceiro(reg.getString("A0015_NM_AGENTE_PARCEIRO"));                                           	
			propostaDTO.setCcAnoReceita(reg.getString("A0930_CC_ANO_RECEITA"));                                                   	
			propostaDTO.setCcReceitaCliente(reg.getString("A0930_CC_RECEITA_CLIENTE"));                                           	
			propostaDTO.setDscReceitaCliente(reg.getString("A0930_DSC_RECEITA_CLIENTE"));                                         	
			propostaDTO.setCcAnoDespesa(reg.getString("A0930_CC_ANO_DESPESA"));                                                   	
			propostaDTO.setCcDespesa(reg.getString("A0930_CC_DESPESA"));                                                          	
			propostaDTO.setDscDespesa(reg.getString("A0930_DSC_DESPESA"));                                                        	
			propostaDTO.setCcDespSaldoDisp(reg.getString("A0930_CC_DESP_SALDO_DISP"));                                            	
			propostaDTO.setCcDespSaldoCompr(reg.getString("A0930_CC_DESP_SALDO_COMPR"));                                          	
			propostaDTO.setCcDespSaldoNovProj(reg.getString("A0930_CC_DESP_SALDO_NOV_PROJ"));                                     	
			propostaDTO.setCcAnOutras(reg.getString("A0930_CC_ANO_OUTRAS"));                                                      	
			propostaDTO.setCcOutrasDespesa(reg.getString("A0930_CC_OUTRAS_DESPESA"));                                             	
			propostaDTO.setDscOutrasDespesa(reg.getString("A0930_DSC_OUTRAS_DESPESA"));                                           	
			propostaDTO.setCcSaldo(reg.getString("A0930_CC_SALDO"));                                                              	
			propostaDTO.setCcAnoSaldo(reg.getString("A0930_CC_ANO_SALDO"));                                                       	
			propostaDTO.setCcDscSaldo(reg.getString("A0930_CC_DSC_SALDO"));                                                       	
			propostaDTO.setVlrSebrae(reg.getString("A0930_VLR_SEBRAE"));                                                          	
			propostaDTO.setVlrSebraeFm(reg.getString("A0930_VLR_SEBRAE_FM"));                                                     	
			propostaDTO.setVlrSebraeFme(reg.getDouble("A0930_VLR_SEBRAE_FME"));                                                   	
			propostaDTO.setVlrSebraeNa(reg.getString("A0930_VLR_SEBRAE_NA"));                                                     	
			propostaDTO.setVlrSebraeNaFm(reg.getString("A0930_VLR_SEBRAE_NA_FM"));                                                	
			propostaDTO.setVlrSebraeNaFme(reg.getDouble("A0930_VLR_SEBRAE_NA_FME"));                                              	
			propostaDTO.setVlrPatrocinado(reg.getString("A0930_VLR_PATROCINADO"));                                                	
			propostaDTO.setVlrPatrocinadoFm(reg.getString("A0930_VLR_PATROCINADO_FM"));                                           	
			propostaDTO.setVlrPatrocinadoFme(reg.getDouble("A0930_VLR_PATROCINADO_FME"));                                         	
			propostaDTO.setVlrParceiro(reg.getString("A0930_VLR_PARCEIRO"));                                                      	
			propostaDTO.setVlrParceiroFm(reg.getString("A0930_VLR_PARCEIRO_FM"));                                                 	
			propostaDTO.setVlrParceiroFme(reg.getDouble("A0930_VLR_PARCEIRO_FME"));                                               	
			propostaDTO.setVlrFixoFme(reg.getDouble("A0930_VLR_FIXO_FME"));                                                       	
			propostaDTO.setIndAltFinanceiro(reg.getBoolean("A0930_IND_ALT_FINANCEIRO", false));                                   	
			propostaDTO.setCodAgenteInc(reg.getString("A0015_COD_AGENTE_INC"));                                                   	
			propostaDTO.setNmAgenteInc(reg.getString("A0015_NM_AGENTE_INC"));                                                     	
			propostaDTO.setCodAgenteAtz(reg.getString("A0015_COD_AGENTE_ATZ"));                                                   	
			propostaDTO.setNmAgenteAtz(reg.getString("A0015_NM_AGENTE_ATZ"));                                                     	
			propostaDTO.setDtInc(reg.getDate("A0930_DT_INC"));                                                                    	
			propostaDTO.setDtIncFm(reg.getDate("A0930_DT_INC_FM"));                                                               	
			propostaDTO.setDtAtz(reg.getDate("A0930_DT_ATZ"));                                                                    	
			propostaDTO.setDtAtzFm(reg.getDate("A0930_DT_ATZ_FM"));                                                               	
			propostaDTO.setIndAtivo(reg.getBoolean("A0930_IND_ATIVO", false));                                                    	
			propostaDTO.setComoConheceSebraetec(reg.getString("A0930_COMO_CONHECE_SEBRAETEC"));                                   	
			propostaDTO.setIndQuestReacao(reg.getBoolean("A0930_IND_QUEST_REACAO", false));                                       	
			propostaDTO.setIndQuestEfetividade(reg.getBoolean("A0930_IND_QUEST_EFETIVIDADE", false));                             	
			propostaDTO.setPossuiEntExtra(reg.getString("POSSUI_ENT_EXTRA"));                                                     	
			propostaDTO.setIndIndividuaColetiva(reg.getBoolean("A0930_IND_INDIVIDUAL_COLETIVA", false ));                          	
			propostaDTO.setIndContabOrient(reg.getBoolean("A0930_IND_CONTAB_ORIENT", false ));                                     	
			propostaDTO.setIndPactoColetivoAssinado(reg.getBoolean("IND_PACTO_COLETIVO_ASSINADO", false));                        	
			propostaDTO.setCodArqPactoColetivo(reg.getString("A0689_COD_ARQ_PACTO_COLETIVO"));                                    	
			propostaDTO.setDtFinParcialFm(reg.getDate("A0930_DT_FIN_PARCIAL_FM"));                                                	
			propostaDTO.setPercFinParcial(reg.getDouble("A0930_PERC_FIN_PARCIAL"));                                               	
			propostaDTO.setJustificativaRecusa(reg.getString("A0930_JUSTIFICATIVA_RECUSA"));                                      	
			propostaDTO.setNecessidadePlan(reg.getString("NECESSIDADE_PLANO"));			
			propostaDTO.setCodEntidade(reg.getLong("COD_ENTIDADE"));
			propostaDTO.setNomeEntidade(reg.getString("NOME_ENTIDADE"));
			propostaDTO.setDscSubServico(reg.getString("DSC_SUBSERVICO"));			
			propostaDTO.setNomeRespEntidade(reg.getString("NOME_RESP_ENTIDADE"));
			propostaDTO.setFoneEntidade(reg.getString("FONE_RESP_ENTIDADE"));
			propostaDTO.setEmailEntidade(reg.getString("EMAIL_ENTIDADE"));
			propostaDTO.setMotivoCancelamento(reg.getString("MOTIVO_CANCELAMENTO"));
			
			
                return propostaDTO;
            }

        };
        final List<PropostaDTO> ret = executarQuery(sql, params, strConverter);

        return ret;
    }
    
    @Override
    public List<AtividadePropostaDTO> pesquisarAtividadesProposta(Long codProposta) {
    	
    	final StringBuilder sql = new StringBuilder();
        sql.append("select t935.A0930_COD_PROPOSTA,    " + 
        		"                     t935.A0015_COD_AGENTE_ENT,    " + 
        		"                     t935.A0935_COD_ATIVIDADE,    " + 
        		"                     t935.A0200_COD_DOMIN,    " + 
        		"                     master.fn_busca_dsc_dominio(t935.a0200_cod_domin) A0200_DSC_DOMIN,    " + 
        		"                     t935.A0935_NM_ATIVIDADE,    " + 
        		"                     REPLACE(t935.A0935_NM_ATIVIDADE, '\"', '-' ) A0935_NM_ATIVIDADE_SEM_ASPAS,    " + 
        		"                     t935.A0935_DSC_ATIVIDADE,    " + 
        		"                     t935.a0935_vlr_hora,    " + 
        		"                     'R$ '||MASTER.PC_STEC.FN_FM_CURRENT_BR(t935.a0935_vlr_hora)  A0935_VLR_HORA_FM,    " + 
        		"                     MASTER.PC_STEC.FN_FM_CURRENT_BR(t935.a0935_vlr_hora)  A0935_VLR_HORA_FME,    " + 
        		"                     t935.A0935_QNT_HORA,    " + 
        		"                     'R$ '||MASTER.PC_STEC.FN_FM_CURRENT_BR(t935.A0935_QNT_HORA*t935.a0935_vlr_hora) A0935_VLR_TOTAL_FM,    " + 
        		"                     t935.a0935_dt_inicio,    " + 
        		"                     to_char(t935.a0935_dt_inicio,'DD/MM/YYYY') A0935_DT_INICIO_FM,    " + 
        		"                     t935.a0935_dt_fim,    " + 
        		"                     to_char(t935.a0935_dt_fim,'DD/MM/YYYY') A0935_DT_FIM_FM,    " + 
        		"                     to_char(t935.a0935_dt_inicio,'DD/MM/YYYY')||' até '||    " + 
        		"                     to_char(t935.a0935_dt_fim,'DD/MM/YYYY') A0935_DT_TAREFA_FM,    " + 
        		"                     to_char(t935.a0935_dt_inicio_original,'DD/MM/YYYY')||' até '||    " + 
        		"                     to_char(t935.a0935_dt_fim_original,'DD/MM/YYYY') A0935_DT_TAREFA_ORIG_FM,    " + 
        		"                     t935.a0015_cod_agente_inc,    " + 
        		"                     t935.a0015_cod_agente_atz,    " + 
        		"                     t935.a0935_dt_inc,    " + 
        		"                     t935.a0935_dt_atz,    " + 
        		"                     to_char(t935.a0935_dt_atz,'DD/MM/YYYY') A0935_DT_ATZ_FM,    " + 
        		"                     to_char(t935.a0935_dt_inc,'DD/MM/YYYY') A0935_DT_INC_FM,    " + 
        		"                     master.fn_busca_nome('agente',t935.a0015_cod_agente_atz) A0015_NM_AGENTE_ATZ,    " + 
        		"                     master.fn_busca_nome('agente',t935.a0015_cod_agente_inc) A0015_NM_AGENTE_INC,    " +
        		"                     case when (t935.A0935_DT_SOLIC_APROV is not null)    " + 
        		"                        then    " + 
        		"                           'Aguardando aprovação da data ('||    " + 
        		"                           to_char(t935.a0935_dt_inicio_aprov,'DD/MM/YYYY')||nvl2(t935.a0935_dt_fim_aprov,' até ', null)||    " + 
        		"                           to_char(t935.a0935_dt_fim_aprov,'DD/MM/YYYY') ||')'    " + 
        		"                        else null end  A0935_DT_TAREFA_PROR_FM,    " + 
        		"                     t935.A0935_DT_INICIO_APROV,    " + 
        		"                     to_char(t935.a0935_dt_inicio_aprov,'DD/MM/YYYY') A0935_DT_INICIO_APROV_FM,    " + 
        		"                     t935.A0935_DT_FIM_APROV,    " + 
        		"                     to_char(t935.a0935_dt_fim_aprov,'DD/MM/YYYY') A0935_DT_FIM_APROV_FM,    " + 
        		"                     t935.A0935_DT_SOLIC_APROV,    " + 
        		"                     to_char(t935.a0935_dt_fim_aprov,'DD/MM/YYYY') A0935_DT_SOLIC_APROV_FM,    " + 
        		"                     t935.A0935_PORC_CONCLUIDO,    " + 
        		"                     t935.a0935_porc_concluido||'%' A0935_PORC_CONCLUIDO_FM,    " +
        		"                     to_char(nvl(t935.a0935_dt_inicio_aprov,t935.a0935_dt_inicio) ,'DD/MM/YYYY') A0935_DT_INICIO_EDICAO_FM,    " + 
        		"                     to_char(nvl(t935.a0935_dt_fim_aprov,t935.a0935_dt_fim) ,'DD/MM/YYYY') A0935_DT_FIM_EDICAO_FM,    " + 
        		"                     t935.a0935_ind_concluido,    " + 
        		"                     case when (t935.A0935_DT_SOLIC_APROV is null) then t935.A0935_IND_ATIVO else 0 end  A0935_IND_ATIVO    " +
        		"                from master.tb0935_st2_prop_atividade t935    " + 
        		"               where t935.a0935_ind_ativo = 1 " +
        		"				 and    t935.a0930_cod_proposta = ? " + 
        		"				 order by t935.a0935_dt_inicio ");

        final List<InParam> params = new ArrayList<InParam>();
        params.add(new InParam(1, codProposta, ParamType.NUMERIC));

        final IConverter<AtividadePropostaDTO> strConverter = new IConverter<AtividadePropostaDTO>() {

            @Override
            public AtividadePropostaDTO convert(ItemResultset reg) {
            	AtividadePropostaDTO atividadePropostaDTO = new AtividadePropostaDTO();
            	
            	atividadePropostaDTO.setCodProposta(reg.getLong("A0930_COD_PROPOSTA"));    
            	atividadePropostaDTO.setCodAgenteEnt(reg.getLong("A0015_COD_AGENTE_ENT")); 
            	atividadePropostaDTO.setCodAtividade(reg.getLong("A0935_COD_ATIVIDADE")); 
            	atividadePropostaDTO.setCodDomin(reg.getLong("A0200_COD_DOMIN")); 
            	atividadePropostaDTO.setDscDomin(reg.getString("A0200_DSC_DOMIN"));
            	atividadePropostaDTO.setNmAtividade(reg.getString("A0935_NM_ATIVIDADE"));
            	atividadePropostaDTO.setNmAtividadeSemAspas(reg.getString("A0935_NM_ATIVIDADE_SEM_ASPAS"));
            	atividadePropostaDTO.setDscStividade(reg.getString("A0935_DSC_ATIVIDADE"));
            	atividadePropostaDTO.setVlrHora(reg.getString("a0935_vlr_hora"));
            	atividadePropostaDTO.setVlrHoraFm(reg.getString("A0935_VLR_HORA_FM"));
            	atividadePropostaDTO.setVlrHoraFme(reg.getString("A0935_VLR_HORA_FME"));
            	atividadePropostaDTO.setQntHora(reg.getLong("A0935_QNT_HORA"));
            	atividadePropostaDTO.setVlrTotalFm(reg.getString("A0935_VLR_TOTAL_FM"));
            	atividadePropostaDTO.setDtInicio(reg.getDate("a0935_dt_inicio"));
            	atividadePropostaDTO.setDtInicioFm(reg.getString("A0935_DT_INICIO_FM"));
            	atividadePropostaDTO.setDtFim(reg.getDate("a0935_dt_fim"));
            	atividadePropostaDTO.setDtFimFm(reg.getString("A0935_DT_FIM_FM"));
            	atividadePropostaDTO.setDtTarefaFm(reg.getString("A0935_DT_TAREFA_FM"));
            	atividadePropostaDTO.setDtTarefaOrigFm(reg.getString("A0935_DT_TAREFA_ORIG_FM"));
            	atividadePropostaDTO.setCodAgenteInc(reg.getLong("a0015_cod_agente_inc"));
            	atividadePropostaDTO.setCodAgenteAtz(reg.getLong("a0015_cod_agente_atz"));
            	atividadePropostaDTO.setDtInc(reg.getDate("a0935_dt_inc"));
            	atividadePropostaDTO.setDtAtz(reg.getDate("a0935_dt_atz"));
            	atividadePropostaDTO.setDtAtzFm(reg.getString("A0935_DT_ATZ_FM"));
            	atividadePropostaDTO.setDtIncFm(reg.getString("A0935_DT_INC_FM"));
            	atividadePropostaDTO.setNmAgenteAtz(reg.getString("A0015_NM_AGENTE_ATZ"));
            	atividadePropostaDTO.setNmAgenteInc(reg.getString("A0015_NM_AGENTE_INC"));
            	atividadePropostaDTO.setDtTarefAprorFm(reg.getString("A0935_DT_TAREFA_PROR_FM"));
            	atividadePropostaDTO.setDtInicioAprov(reg.getDate("A0935_DT_INICIO_APROV"));
            	atividadePropostaDTO.setDtInicioAprovFm(reg.getString("A0935_DT_INICIO_APROV_FM"));
            	atividadePropostaDTO.setDtFimAprov(reg.getDate("A0935_DT_FIM_APROV"));
            	atividadePropostaDTO.setDtFimAprovFm(reg.getString("A0935_DT_FIM_APROV_FM"));
            	atividadePropostaDTO.setDtSolicAprov(reg.getDate("A0935_DT_SOLIC_APROV"));
            	atividadePropostaDTO.setDtSolicAprovFm(reg.getString("A0935_DT_SOLIC_APROV_FM"));
            	atividadePropostaDTO.setPorcConcluido(reg.getLong("A0935_PORC_CONCLUIDO"));
            	atividadePropostaDTO.setPorcConcluidoFm(reg.getString("A0935_PORC_CONCLUIDO_FM"));
            	atividadePropostaDTO.setDtInicioEdicaoFm(reg.getString("A0935_DT_INICIO_EDICAO_FM"));
            	atividadePropostaDTO.setDtFimEdicaoFm(reg.getString("A0935_DT_FIM_EDICAO_FM"));
            	atividadePropostaDTO.setIndConcluido(reg.getBoolean("a0935_ind_concluido", false));
            	atividadePropostaDTO.setIndAtivo(reg.getBoolean("A0935_IND_ATIVO", false));
            
                return atividadePropostaDTO;
            }

        };
        final List<AtividadePropostaDTO> ret = executarQuery(sql, params, strConverter);

        return ret;
    }
    
    @Override
    public void recusarProposta(Long codProposta, Long codCliente, String justificativa) {
    	final StringBuilder call = new StringBuilder();
        call.append("BEGIN update master.tb0930_st2_proposta t930 " + 
        		"       set t930.a0939_cod_status = '10,5', " + 
        		"          t930.a0930_justificativa = ?, " +
        		"          t930.a0015_cod_agente_atz = ?, " +
        		"          t930.a0930_dt_atz = sysdate " +
        		"       where t930.a0930_cod_proposta = ?; END;");
        
        CallableStatement stmt = null;
        Connection conn = null;
        
        try {
            conn = this.getConnection();
            stmt = conn.prepareCall(call.toString());
            
            stmt.setString(1, justificativa);
            stmt.setLong(2, codCliente);
            stmt.setLong(3, codProposta);
            
            stmt.executeUpdate();
            
        } catch (Exception e) {
            e.printStackTrace();
            throw new BadRequestException("Erro ao recusar proposta pelo cliente via área logada!");
        } finally {
            try {
                stmt.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                throw new BadRequestException("Erro ao recusar proposta pelo cliente via área logada!");
            }
        }
    }
    
    @Override
    public void aprovarProposta(Long codProposta, Long codCliente) {
    	final StringBuilder call = new StringBuilder();
        call.append("BEGIN update master.tb0930_st2_proposta t930 " + 
        		"       set t930.a0939_cod_status = '10', " +
        		"          t930.a0015_cod_agente_atz = ?, " +
        		"          t930.a0930_dt_atz = sysdate " +
        		"       where t930.a0930_cod_proposta = ?; END;");
        
        CallableStatement stmt = null;
        Connection conn = null;
        
        try {
            conn = this.getConnection();
            stmt = conn.prepareCall(call.toString());
            
            stmt.setLong(1, codCliente);
            stmt.setLong(2, codProposta);
            
            stmt.executeUpdate();
            
        } catch (Exception e) {
            e.printStackTrace();
            throw new BadRequestException("Erro ao aprovar proposta pelo cliente via área logada!");
        } finally {
            try {
                stmt.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                throw new BadRequestException("Erro ao aprovar proposta pelo cliente via área logada!");
            }
        }
    }
    
    @Override
    public void manterHitoricoProposta(Long codProposta, Long codCliente, String acao) {
    	
    	final StringBuilder call = new StringBuilder();
        call.append("begin master.pc_stec_proposta.SP_MANTER_PROP_HIST(?, ?, ?); end;");
        
        CallableStatement stmt = null;
        Connection conn = null;
        
        try {
            conn = this.getConnection();
            stmt = conn.prepareCall(call.toString());
            
            stmt.setLong(1, codProposta);
            stmt.setString(2, acao);
            stmt.setLong(3, codCliente);
            
            
            stmt.executeUpdate();
            
        } catch (Exception e) {
            e.printStackTrace();
            throw new BadRequestException("Erro ao atualizar histórico!");
        } finally {
            try {
                stmt.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                throw new BadRequestException("Erro ao atualizar histórico!");
            }
        }
    }
    
    @Override
    public void recusarPlanoTrabalho(Long codProposta, Long codCliente) {
    	final StringBuilder call = new StringBuilder();
        call.append("BEGIN update master.tb0930_st2_proposta t930 " + 
        		"       set t930.a0939_cod_status = '11', " +
        		"          t930.a0015_cod_agente_atz = ?, " +
        		"          t930.a0930_dt_atz = sysdate " +
        		"       where t930.a0930_cod_proposta = ?; END;");
        
        CallableStatement stmt = null;
        Connection conn = null;
        
        try {
            conn = this.getConnection();
            stmt = conn.prepareCall(call.toString());
            
            stmt.setLong(1, codCliente);
            stmt.setLong(2, codProposta);
            
            stmt.executeUpdate();
            
        } catch (Exception e) {
            e.printStackTrace();
            throw new BadRequestException("Erro ao recusar plano de trabalho pelo cliente via área logada!");
        } finally {
            try {
                stmt.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                throw new BadRequestException("Erro ao recusar plano de trabalho pelo cliente via área logada!");
            }
        }
    }
    
    @Override
    public void aprovarPlanoTrabalho(Long codProposta, Long codCliente) {
    	final StringBuilder call = new StringBuilder();
        call.append("BEGIN update master.tb0930_st2_proposta t930 " + 
        		"       set t930.a0939_cod_status = '13', " +
        		"          t930.a0015_cod_agente_atz = ?, " +
        		"          t930.a0930_dt_atz = sysdate " +
        		"       where t930.a0930_cod_proposta = ?; END;");
        
        CallableStatement stmt = null;
        Connection conn = null;
        
        try {
            conn = this.getConnection();
            stmt = conn.prepareCall(call.toString());
            
            stmt.setLong(1, codCliente);
            stmt.setLong(2, codProposta);
            
            stmt.executeUpdate();
            
        } catch (Exception e) {
            e.printStackTrace();
            throw new BadRequestException("Erro ao aprovar plano de trabalho pelo cliente via área logada!");
        } finally {
            try {
                stmt.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                throw new BadRequestException("Erro ao aprovar plano de trabalho pelo cliente via área logada!");
            }
        }
    }
    
    @Override
    public void comentarProposta(Long codProposta, Long codCliente, String comentario) {
    	
    	final StringBuilder call = new StringBuilder();
        call.append("BEGIN master.pc_stec_proposta.sp_manter_prop_comment(?,   " + 
        		"                2,   " + 
        		"                ?,  " +  
        		"                ?); END;");
        
        CallableStatement stmt = null;
        Connection conn = null;
        
        try {
            conn = this.getConnection();
            stmt = conn.prepareCall(call.toString());
            
            stmt.setLong(1, codProposta);
            stmt.setString(2, comentario);
            stmt.setLong(3, codCliente);
            
            stmt.executeUpdate();
            
        } catch (Exception e) {
            e.printStackTrace();
            throw new BadRequestException("Erro ao incluir comentario pelo cliente via área logada!");
        } finally {
            try {
                stmt.close();
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                throw new BadRequestException("Erro ao incluir comentario pelo cliente via área logada!");
            }
        }
    }
    
    
    
}
