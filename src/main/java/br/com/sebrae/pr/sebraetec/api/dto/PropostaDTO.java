package br.com.sebrae.pr.sebraetec.api.dto;

import java.util.Date;
import java.util.List;

import br.com.wise.boot.starter.dto.base.LongIdBaseDTO;

public class PropostaDTO extends LongIdBaseDTO {

	private static final long serialVersionUID = -5726398863199195674L;
	
	private Long codCli;
    private String nomeCliente;
    private Long codProposta;
    private Long codPropostaOrigem;
    private Long seqContinuidade;
    private Long codStatus;
    private String labelStatus;
    private Long codCategoria;
    private String dscCategoria;
    private boolean indInovacao;
    private Long codTipoDemanda;
    private String dscTipoDemanda;
    private Long tipoContab;
    private Long codServico;
    private String dscPropServico;
    private Long codCcusto;
    private String ccDescricao;
    private String ccAno;
    private String ccSaldoDisp;
    private String ccSaldoComprometido;
    private String ccSaldoNovProj;
    private Long vlrProposta;
    private String vlrPropostaFm;    
    private Long vlrCliente;
    private String vlrClienteFm;
    private Long vlrContrapartida;
    private String vlrContrapartidaFm;
    private String necessidade;
    private Long nrClientesPrevistos;
    private Long codAgenteSolic;
    private String nmAgenteSolic;
    private String telefoneSolic;
    private String emailSolic;
    private Long codAgenteResp;
    private String nmAgenteResp;
    private String telefoneResp;
    private String emailResp;
    private String dscSolucao;
    private String dscResultado;
    private String justificativa;
    private String cancelamento;
    private Long codAgenteGer;
    private String nmAgenteGer;
    private String telefoneGer;
    private Long codAgentesComite;
    private Long codAgentesUic;
    private Date dtValPlanoResp;
    private Date dtValPlanoRespFm;
    private Date dtValPlanoGerente;
    private Date dtValPlanoGerenteFm;
    private Date dtValPlanoComite;
    private Date dtValPlanoComiteFm;
    private Long codUnid;
    private String dscUnid;
    private Long codAgentePatrocinador;
    private String nmAgentePatrocinador;
    private String ccPatrocinador;
    private String ccAnoPatrocinador;
    private String dscCcPatrocinador;
    private String ccPatrocinadorReceita;
    private String dscCcPatrocinadorRec;
    private String ccAnoPatrocinadorRec;
    private String codAgenteParceiro;
    private String nmAgenteParceiro;
    private String ccAnoReceita;
    private String ccReceitaCliente;
    private String dscReceitaCliente;
    private String ccAnoDespesa;
    private String ccDespesa;
    private String dscDespesa;
    private String ccDespSaldoDisp;
    private String ccDespSaldoCompr;
    private String ccDespSaldoNovProj;
    private String ccAnOutras;
    private String ccOutrasDespesa;
    private String dscOutrasDespesa;
    private String ccSaldo;
    private String ccAnoSaldo;
    private String ccDscSaldo;
    private String vlrSebrae;
    private String vlrSebraeFm;
    private Double vlrSebraeFme;
    private String vlrSebraeNa;
    private String vlrSebraeNaFm;
    private Double vlrSebraeNaFme;
    private String vlrPatrocinado;
    private String vlrPatrocinadoFm;
    private Double vlrPatrocinadoFme;
    private String vlrParceiro;
    private String vlrParceiroFm;
    private Double vlrParceiroFme;
    private Double vlrFixoFme;
    private boolean indAltFinanceiro;
    private String codAgenteInc;
    private String nmAgenteInc;
    private String codAgenteAtz;
    private String nmAgenteAtz;
    private Date dtInc;
    private Date dtIncFm;
    private Date dtAtz;
    private Date dtAtzFm;
    private boolean indAtivo;
    private String comoConheceSebraetec;
    private boolean indQuestReacao;
    private boolean indQuestEfetividade;
    private String possuiEntExtra;
    private boolean indIndividuaColetiva;
    private boolean indContabOrient;
    private boolean indPactoColetivoAssinado;
    private String codArqPactoColetivo;
    private Date dtFinParcialFm;
    private Double percFinParcial;
    private String justificativaRecusa;
    private String necessidadePlan;
    private Long codEntidade;
    private String nomeEntidade;
    private List<AtividadePropostaDTO> listaAtividade;
    private String dscSubServico;
    private String nomeRespEntidade;
    private String foneEntidade;
    private String emailEntidade;
    private String motivoCancelamento;
    
    public PropostaDTO() {
    }

	public Long getCodCli() {
		return codCli;
	}

	public void setCodCli(Long codCli) {
		this.codCli = codCli;
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public Long getCodProposta() {
		return codProposta;
	}

	public void setCodProposta(Long codProposta) {
		this.codProposta = codProposta;
	}

	public Long getCodPropostaOrigem() {
		return codPropostaOrigem;
	}

	public void setCodPropostaOrigem(Long codPropostaOrigem) {
		this.codPropostaOrigem = codPropostaOrigem;
	}

	public Long getSeqContinuidade() {
		return seqContinuidade;
	}

	public void setSeqContinuidade(Long seqContinuidade) {
		this.seqContinuidade = seqContinuidade;
	}

	public Long getCodStatus() {
		return codStatus;
	}

	public void setCodStatus(Long codStatus) {
		this.codStatus = codStatus;
	}

	public String getLabelStatus() {
		return labelStatus;
	}

	public void setLabelStatus(String labelStatus) {
		this.labelStatus = labelStatus;
	}

	public Long getCodCategoria() {
		return codCategoria;
	}

	public void setCodCategoria(Long codCategoria) {
		this.codCategoria = codCategoria;
	}

	public String getDscCategoria() {
		return dscCategoria;
	}

	public void setDscCategoria(String dscCategoria) {
		this.dscCategoria = dscCategoria;
	}

	public boolean isIndInovacao() {
		return indInovacao;
	}

	public void setIndInovacao(boolean indInovacao) {
		this.indInovacao = indInovacao;
	}

	public Long getCodTipoDemanda() {
		return codTipoDemanda;
	}

	public void setCodTipoDemanda(Long codTipoDemanda) {
		this.codTipoDemanda = codTipoDemanda;
	}

	public String getDscTipoDemanda() {
		return dscTipoDemanda;
	}

	public void setDscTipoDemanda(String dscTipoDemanda) {
		this.dscTipoDemanda = dscTipoDemanda;
	}

	public Long getTipoContab() {
		return tipoContab;
	}

	public void setTipoContab(Long tipoContab) {
		this.tipoContab = tipoContab;
	}

	public Long getCodServico() {
		return codServico;
	}

	public void setCodServico(Long codServico) {
		this.codServico = codServico;
	}

	public String getDscPropServico() {
		return dscPropServico;
	}

	public void setDscPropServico(String dscPropServico) {
		this.dscPropServico = dscPropServico;
	}

	public Long getCodCcusto() {
		return codCcusto;
	}

	public void setCodCcusto(Long codCcusto) {
		this.codCcusto = codCcusto;
	}

	public String getCcDescricao() {
		return ccDescricao;
	}

	public void setCcDescricao(String ccDescricao) {
		this.ccDescricao = ccDescricao;
	}

	public String getCcAno() {
		return ccAno;
	}

	public void setCcAno(String ccAno) {
		this.ccAno = ccAno;
	}

	public String getCcSaldoDisp() {
		return ccSaldoDisp;
	}

	public void setCcSaldoDisp(String ccSaldoDisp) {
		this.ccSaldoDisp = ccSaldoDisp;
	}

	public String getCcSaldoComprometido() {
		return ccSaldoComprometido;
	}

	public void setCcSaldoComprometido(String ccSaldoComprometido) {
		this.ccSaldoComprometido = ccSaldoComprometido;
	}

	public String getCcSaldoNovProj() {
		return ccSaldoNovProj;
	}

	public void setCcSaldoNovProj(String ccSaldoNovProj) {
		this.ccSaldoNovProj = ccSaldoNovProj;
	}

	public Long getVlrProposta() {
		return vlrProposta;
	}

	public void setVlrProposta(Long vlrProposta) {
		this.vlrProposta = vlrProposta;
	}

	public Long getVlrCliente() {
		return vlrCliente;
	}

	public void setVlrCliente(Long vlrCliente) {
		this.vlrCliente = vlrCliente;
	}

	public Long getVlrContrapartida() {
		return vlrContrapartida;
	}

	public void setVlrContrapartida(Long vlrContrapartida) {
		this.vlrContrapartida = vlrContrapartida;
	}

	public String getNecessidade() {
		return necessidade;
	}

	public void setNecessidade(String necessidade) {
		this.necessidade = necessidade;
	}

	public Long getNrClientesPrevistos() {
		return nrClientesPrevistos;
	}

	public void setNrClientesPrevistos(Long nrClientesPrevistos) {
		this.nrClientesPrevistos = nrClientesPrevistos;
	}

	public Long getCodAgenteSolic() {
		return codAgenteSolic;
	}

	public void setCodAgenteSolic(Long codAgenteSolic) {
		this.codAgenteSolic = codAgenteSolic;
	}

	public String getNmAgenteSolic() {
		return nmAgenteSolic;
	}

	public void setNmAgenteSolic(String nmAgenteSolic) {
		this.nmAgenteSolic = nmAgenteSolic;
	}

	public String getTelefoneSolic() {
		return telefoneSolic;
	}

	public void setTelefoneSolic(String telefoneSolic) {
		this.telefoneSolic = telefoneSolic;
	}

	public String getEmailSolic() {
		return emailSolic;
	}

	public void setEmailSolic(String emailSolic) {
		this.emailSolic = emailSolic;
	}

	public Long getCodAgenteResp() {
		return codAgenteResp;
	}

	public void setCodAgenteResp(Long codAgenteResp) {
		this.codAgenteResp = codAgenteResp;
	}

	public String getNmAgenteResp() {
		return nmAgenteResp;
	}

	public void setNmAgenteResp(String nmAgenteResp) {
		this.nmAgenteResp = nmAgenteResp;
	}

	public String getTelefoneResp() {
		return telefoneResp;
	}

	public void setTelefoneResp(String telefoneResp) {
		this.telefoneResp = telefoneResp;
	}

	public String getEmailResp() {
		return emailResp;
	}

	public void setEmailResp(String emailResp) {
		this.emailResp = emailResp;
	}

	public String getDscSolucao() {
		return dscSolucao;
	}

	public void setDscSolucao(String dscSolucao) {
		this.dscSolucao = dscSolucao;
	}

	public String getDscResultado() {
		return dscResultado;
	}

	public void setDscResultado(String dscResultado) {
		this.dscResultado = dscResultado;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}

	public String getCancelamento() {
		return cancelamento;
	}

	public void setCancelamento(String cancelamento) {
		this.cancelamento = cancelamento;
	}

	public Long getCodAgenteGer() {
		return codAgenteGer;
	}

	public void setCodAgenteGer(Long codAgenteGer) {
		this.codAgenteGer = codAgenteGer;
	}

	public String getNmAgenteGer() {
		return nmAgenteGer;
	}

	public void setNmAgenteGer(String nmAgenteGer) {
		this.nmAgenteGer = nmAgenteGer;
	}

	public String getTelefoneGer() {
		return telefoneGer;
	}

	public void setTelefoneGer(String telefoneGer) {
		this.telefoneGer = telefoneGer;
	}

	public Long getCodAgentesComite() {
		return codAgentesComite;
	}

	public void setCodAgentesComite(Long codAgentesComite) {
		this.codAgentesComite = codAgentesComite;
	}

	public Long getCodAgentesUic() {
		return codAgentesUic;
	}

	public void setCodAgentesUic(Long codAgentesUic) {
		this.codAgentesUic = codAgentesUic;
	}

	public Date getDtValPlanoResp() {
		return dtValPlanoResp;
	}

	public void setDtValPlanoResp(Date dtValPlanoResp) {
		this.dtValPlanoResp = dtValPlanoResp;
	}

	public Date getDtValPlanoRespFm() {
		return dtValPlanoRespFm;
	}

	public void setDtValPlanoRespFm(Date dtValPlanoRespFm) {
		this.dtValPlanoRespFm = dtValPlanoRespFm;
	}

	public Date getDtValPlanoGerente() {
		return dtValPlanoGerente;
	}

	public void setDtValPlanoGerente(Date dtValPlanoGerente) {
		this.dtValPlanoGerente = dtValPlanoGerente;
	}

	public Date getDtValPlanoGerenteFm() {
		return dtValPlanoGerenteFm;
	}

	public void setDtValPlanoGerenteFm(Date dtValPlanoGerenteFm) {
		this.dtValPlanoGerenteFm = dtValPlanoGerenteFm;
	}

	public Date getDtValPlanoComite() {
		return dtValPlanoComite;
	}

	public void setDtValPlanoComite(Date dtValPlanoComite) {
		this.dtValPlanoComite = dtValPlanoComite;
	}

	public Date getDtValPlanoComiteFm() {
		return dtValPlanoComiteFm;
	}

	public void setDtValPlanoComiteFm(Date dtValPlanoComiteFm) {
		this.dtValPlanoComiteFm = dtValPlanoComiteFm;
	}

	public Long getCodUnid() {
		return codUnid;
	}

	public void setCodUnid(Long codUnid) {
		this.codUnid = codUnid;
	}

	public String getDscUnid() {
		return dscUnid;
	}

	public void setDscUnid(String dscUnid) {
		this.dscUnid = dscUnid;
	}

	public Long getCodAgentePatrocinador() {
		return codAgentePatrocinador;
	}

	public void setCodAgentePatrocinador(Long codAgentePatrocinador) {
		this.codAgentePatrocinador = codAgentePatrocinador;
	}

	public String getNmAgentePatrocinador() {
		return nmAgentePatrocinador;
	}

	public void setNmAgentePatrocinador(String nmAgentePatrocinador) {
		this.nmAgentePatrocinador = nmAgentePatrocinador;
	}

	public String getCcPatrocinador() {
		return ccPatrocinador;
	}

	public void setCcPatrocinador(String ccPatrocinador) {
		this.ccPatrocinador = ccPatrocinador;
	}

	public String getCcAnoPatrocinador() {
		return ccAnoPatrocinador;
	}

	public void setCcAnoPatrocinador(String ccAnoPatrocinador) {
		this.ccAnoPatrocinador = ccAnoPatrocinador;
	}

	public String getDscCcPatrocinador() {
		return dscCcPatrocinador;
	}

	public void setDscCcPatrocinador(String dscCcPatrocinador) {
		this.dscCcPatrocinador = dscCcPatrocinador;
	}

	public String getCcPatrocinadorReceita() {
		return ccPatrocinadorReceita;
	}

	public void setCcPatrocinadorReceita(String ccPatrocinadorReceita) {
		this.ccPatrocinadorReceita = ccPatrocinadorReceita;
	}

	public String getDscCcPatrocinadorRec() {
		return dscCcPatrocinadorRec;
	}

	public void setDscCcPatrocinadorRec(String dscCcPatrocinadorRec) {
		this.dscCcPatrocinadorRec = dscCcPatrocinadorRec;
	}

	public String getCcAnoPatrocinadorRec() {
		return ccAnoPatrocinadorRec;
	}

	public void setCcAnoPatrocinadorRec(String ccAnoPatrocinadorRec) {
		this.ccAnoPatrocinadorRec = ccAnoPatrocinadorRec;
	}

	public String getCodAgenteParceiro() {
		return codAgenteParceiro;
	}

	public void setCodAgenteParceiro(String codAgenteParceiro) {
		this.codAgenteParceiro = codAgenteParceiro;
	}

	public String getNmAgenteParceiro() {
		return nmAgenteParceiro;
	}

	public void setNmAgenteParceiro(String nmAgenteParceiro) {
		this.nmAgenteParceiro = nmAgenteParceiro;
	}

	public String getCcAnoReceita() {
		return ccAnoReceita;
	}

	public void setCcAnoReceita(String ccAnoReceita) {
		this.ccAnoReceita = ccAnoReceita;
	}

	public String getCcReceitaCliente() {
		return ccReceitaCliente;
	}

	public void setCcReceitaCliente(String ccReceitaCliente) {
		this.ccReceitaCliente = ccReceitaCliente;
	}

	public String getDscReceitaCliente() {
		return dscReceitaCliente;
	}

	public void setDscReceitaCliente(String dscReceitaCliente) {
		this.dscReceitaCliente = dscReceitaCliente;
	}

	public String getCcAnoDespesa() {
		return ccAnoDespesa;
	}

	public void setCcAnoDespesa(String ccAnoDespesa) {
		this.ccAnoDespesa = ccAnoDespesa;
	}

	public String getCcDespesa() {
		return ccDespesa;
	}

	public void setCcDespesa(String ccDespesa) {
		this.ccDespesa = ccDespesa;
	}

	public String getDscDespesa() {
		return dscDespesa;
	}

	public void setDscDespesa(String dscDespesa) {
		this.dscDespesa = dscDespesa;
	}

	public String getCcDespSaldoDisp() {
		return ccDespSaldoDisp;
	}

	public void setCcDespSaldoDisp(String ccDespSaldoDisp) {
		this.ccDespSaldoDisp = ccDespSaldoDisp;
	}

	public String getCcDespSaldoCompr() {
		return ccDespSaldoCompr;
	}

	public void setCcDespSaldoCompr(String ccDespSaldoCompr) {
		this.ccDespSaldoCompr = ccDespSaldoCompr;
	}

	public String getCcDespSaldoNovProj() {
		return ccDespSaldoNovProj;
	}

	public void setCcDespSaldoNovProj(String ccDespSaldoNovProj) {
		this.ccDespSaldoNovProj = ccDespSaldoNovProj;
	}

	public String getCcAnOutras() {
		return ccAnOutras;
	}

	public void setCcAnOutras(String ccAnOutras) {
		this.ccAnOutras = ccAnOutras;
	}

	public String getCcOutrasDespesa() {
		return ccOutrasDespesa;
	}

	public void setCcOutrasDespesa(String ccOutrasDespesa) {
		this.ccOutrasDespesa = ccOutrasDespesa;
	}

	public String getDscOutrasDespesa() {
		return dscOutrasDespesa;
	}

	public void setDscOutrasDespesa(String dscOutrasDespesa) {
		this.dscOutrasDespesa = dscOutrasDespesa;
	}

	public String getCcSaldo() {
		return ccSaldo;
	}

	public void setCcSaldo(String ccSaldo) {
		this.ccSaldo = ccSaldo;
	}

	public String getCcAnoSaldo() {
		return ccAnoSaldo;
	}

	public void setCcAnoSaldo(String ccAnoSaldo) {
		this.ccAnoSaldo = ccAnoSaldo;
	}

	public String getCcDscSaldo() {
		return ccDscSaldo;
	}

	public void setCcDscSaldo(String ccDscSaldo) {
		this.ccDscSaldo = ccDscSaldo;
	}

	public String getVlrSebrae() {
		return vlrSebrae;
	}

	public void setVlrSebrae(String vlrSebrae) {
		this.vlrSebrae = vlrSebrae;
	}

	public String getVlrSebraeFm() {
		return vlrSebraeFm;
	}

	public void setVlrSebraeFm(String vlrSebraeFm) {
		this.vlrSebraeFm = vlrSebraeFm;
	}

	public Double getVlrSebraeFme() {
		return vlrSebraeFme;
	}

	public void setVlrSebraeFme(Double vlrSebraeFme) {
		this.vlrSebraeFme = vlrSebraeFme;
	}

	public String getVlrSebraeNa() {
		return vlrSebraeNa;
	}

	public void setVlrSebraeNa(String vlrSebraeNa) {
		this.vlrSebraeNa = vlrSebraeNa;
	}

	public String getVlrSebraeNaFm() {
		return vlrSebraeNaFm;
	}

	public void setVlrSebraeNaFm(String vlrSebraeNaFm) {
		this.vlrSebraeNaFm = vlrSebraeNaFm;
	}

	public Double getVlrSebraeNaFme() {
		return vlrSebraeNaFme;
	}

	public void setVlrSebraeNaFme(Double vlrSebraeNaFme) {
		this.vlrSebraeNaFme = vlrSebraeNaFme;
	}

	public String getVlrPatrocinado() {
		return vlrPatrocinado;
	}

	public void setVlrPatrocinado(String vlrPatrocinado) {
		this.vlrPatrocinado = vlrPatrocinado;
	}

	public String getVlrPatrocinadoFm() {
		return vlrPatrocinadoFm;
	}

	public void setVlrPatrocinadoFm(String vlrPatrocinadoFm) {
		this.vlrPatrocinadoFm = vlrPatrocinadoFm;
	}

	public Double getVlrPatrocinadoFme() {
		return vlrPatrocinadoFme;
	}

	public void setVlrPatrocinadoFme(Double vlrPatrocinadoFme) {
		this.vlrPatrocinadoFme = vlrPatrocinadoFme;
	}

	public String getVlrParceiro() {
		return vlrParceiro;
	}

	public void setVlrParceiro(String vlrParceiro) {
		this.vlrParceiro = vlrParceiro;
	}

	public String getVlrParceiroFm() {
		return vlrParceiroFm;
	}

	public void setVlrParceiroFm(String vlrParceiroFm) {
		this.vlrParceiroFm = vlrParceiroFm;
	}

	public Double getVlrParceiroFme() {
		return vlrParceiroFme;
	}

	public void setVlrParceiroFme(Double vlrParceiroFme) {
		this.vlrParceiroFme = vlrParceiroFme;
	}

	public Double getVlrFixoFme() {
		return vlrFixoFme;
	}

	public void setVlrFixoFme(Double vlrFixoFme) {
		this.vlrFixoFme = vlrFixoFme;
	}

	public boolean isIndAltFinanceiro() {
		return indAltFinanceiro;
	}

	public void setIndAltFinanceiro(boolean indAltFinanceiro) {
		this.indAltFinanceiro = indAltFinanceiro;
	}

	public String getCodAgenteInc() {
		return codAgenteInc;
	}

	public void setCodAgenteInc(String codAgenteInc) {
		this.codAgenteInc = codAgenteInc;
	}

	public String getNmAgenteInc() {
		return nmAgenteInc;
	}

	public void setNmAgenteInc(String nmAgenteInc) {
		this.nmAgenteInc = nmAgenteInc;
	}

	public String getCodAgenteAtz() {
		return codAgenteAtz;
	}

	public void setCodAgenteAtz(String codAgenteAtz) {
		this.codAgenteAtz = codAgenteAtz;
	}

	public String getNmAgenteAtz() {
		return nmAgenteAtz;
	}

	public void setNmAgenteAtz(String nmAgenteAtz) {
		this.nmAgenteAtz = nmAgenteAtz;
	}

	public Date getDtInc() {
		return dtInc;
	}

	public void setDtInc(Date dtInc) {
		this.dtInc = dtInc;
	}

	public Date getDtIncFm() {
		return dtIncFm;
	}

	public void setDtIncFm(Date dtIncFm) {
		this.dtIncFm = dtIncFm;
	}

	public Date getDtAtz() {
		return dtAtz;
	}

	public void setDtAtz(Date dtAtz) {
		this.dtAtz = dtAtz;
	}

	public Date getDtAtzFm() {
		return dtAtzFm;
	}

	public void setDtAtzFm(Date dtAtzFm) {
		this.dtAtzFm = dtAtzFm;
	}

	public boolean isIndAtivo() {
		return indAtivo;
	}

	public void setIndAtivo(boolean indAtivo) {
		this.indAtivo = indAtivo;
	}

	public String getComoConheceSebraetec() {
		return comoConheceSebraetec;
	}

	public void setComoConheceSebraetec(String comoConheceSebraetec) {
		this.comoConheceSebraetec = comoConheceSebraetec;
	}

	public boolean isIndQuestReacao() {
		return indQuestReacao;
	}

	public void setIndQuestReacao(boolean indQuestReacao) {
		this.indQuestReacao = indQuestReacao;
	}

	public boolean isIndQuestEfetividade() {
		return indQuestEfetividade;
	}

	public void setIndQuestEfetividade(boolean indQuestEfetividade) {
		this.indQuestEfetividade = indQuestEfetividade;
	}

	public String getPossuiEntExtra() {
		return possuiEntExtra;
	}

	public void setPossuiEntExtra(String possuiEntExtra) {
		this.possuiEntExtra = possuiEntExtra;
	}

	public boolean isIndIndividuaColetiva() {
		return indIndividuaColetiva;
	}

	public void setIndIndividuaColetiva(boolean indIndividuaColetiva) {
		this.indIndividuaColetiva = indIndividuaColetiva;
	}

	public boolean isIndContabOrient() {
		return indContabOrient;
	}

	public void setIndContabOrient(boolean indContabOrient) {
		this.indContabOrient = indContabOrient;
	}

	public boolean isIndPactoColetivoAssinado() {
		return indPactoColetivoAssinado;
	}

	public void setIndPactoColetivoAssinado(boolean indPactoColetivoAssinado) {
		this.indPactoColetivoAssinado = indPactoColetivoAssinado;
	}

	public String getCodArqPactoColetivo() {
		return codArqPactoColetivo;
	}

	public void setCodArqPactoColetivo(String codArqPactoColetivo) {
		this.codArqPactoColetivo = codArqPactoColetivo;
	}

	public Date getDtFinParcialFm() {
		return dtFinParcialFm;
	}

	public void setDtFinParcialFm(Date dtFinParcialFm) {
		this.dtFinParcialFm = dtFinParcialFm;
	}

	public Double getPercFinParcial() {
		return percFinParcial;
	}

	public void setPercFinParcial(Double percFinParcial) {
		this.percFinParcial = percFinParcial;
	}

	public String getJustificativaRecusa() {
		return justificativaRecusa;
	}

	public void setJustificativaRecusa(String justificativaRecusa) {
		this.justificativaRecusa = justificativaRecusa;
	}

	public String getNecessidadePlan() {
		return necessidadePlan;
	}

	public void setNecessidadePlan(String necessidadePlan) {
		this.necessidadePlan = necessidadePlan;
	}

	public String getNomeEntidade() {
		return nomeEntidade;
	}

	public void setNomeEntidade(String nomeEntidade) {
		this.nomeEntidade = nomeEntidade;
	}

	public String getVlrPropostaFm() {
		return vlrPropostaFm;
	}

	public void setVlrPropostaFm(String vlrPropostaFm) {
		this.vlrPropostaFm = vlrPropostaFm;
	}

	public String getVlrClienteFm() {
		return vlrClienteFm;
	}

	public void setVlrClienteFm(String vlrClienteFm) {
		this.vlrClienteFm = vlrClienteFm;
	}

	public String getVlrContrapartidaFm() {
		return vlrContrapartidaFm;
	}

	public void setVlrContrapartidaFm(String vlrContrapartidaFm) {
		this.vlrContrapartidaFm = vlrContrapartidaFm;
	}

	public List<AtividadePropostaDTO> getListaAtividade() {
		return listaAtividade;
	}

	public void setListaAtividade(List<AtividadePropostaDTO> listaAtividade) {
		this.listaAtividade = listaAtividade;
	}

	public String getDscSubServico() {
		return dscSubServico;
	}

	public void setDscSubServico(String dscSubServico) {
		this.dscSubServico = dscSubServico;
	}

	public String getNomeRespEntidade() {
		return nomeRespEntidade;
	}

	public void setNomeRespEntidade(String nomeRespEntidade) {
		this.nomeRespEntidade = nomeRespEntidade;
	}

	public String getFoneEntidade() {
		return foneEntidade;
	}

	public void setFoneEntidade(String foneEntidade) {
		this.foneEntidade = foneEntidade;
	}

	public String getEmailEntidade() {
		return emailEntidade;
	}

	public void setEmailEntidade(String emailEntidade) {
		this.emailEntidade = emailEntidade;
	}

	public String getMotivoCancelamento() {
		return motivoCancelamento;
	}

	public void setMotivoCancelamento(String motivoCancelamento) {
		this.motivoCancelamento = motivoCancelamento;
	}

	public Long getCodEntidade() {
		return codEntidade;
	}

	public void setCodEntidade(Long codEntidade) {
		this.codEntidade = codEntidade;
	}
	
}
