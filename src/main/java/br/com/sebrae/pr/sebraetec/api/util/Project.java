package br.com.sebrae.pr.sebraetec.api.util;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "project")
public class Project {

	@JacksonXmlProperty(isAttribute = true, localName = "name")
	private String projectName;
	private String version;
	private String buildTimestamp;
	private String profile;

	public Project() {
		super();
	}

	public Project(final String projectName, final String version, final String buildTimestamp, final String profile) {
		super();
		this.projectName = projectName;
		this.version = version;
		this.buildTimestamp = buildTimestamp;
		this.profile = profile;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getBuildTimestamp() {
		return buildTimestamp;
	}

	public void setBuildTimestamp(String buildTimestamp) {
		this.buildTimestamp = buildTimestamp;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

}
