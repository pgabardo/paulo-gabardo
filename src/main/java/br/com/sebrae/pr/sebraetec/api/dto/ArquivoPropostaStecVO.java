package br.com.sebrae.pr.sebraetec.api.dto;

import java.io.Serializable;

public class ArquivoPropostaStecVO implements Serializable {

    private static final long serialVersionUID = -7079637118522420911L;
    private String codigoProposta = "";
    private String codigoEntidade = "";
    private String codigoArquivo = "";
    private String tituloArquivo = "";
    private String nomeArquivo = "";
    private byte[] anexo = null;
    private int tamanhoArquivo;

    private String codigoStatusTermo = "";
    private String codigoTipoTermo = "";
    private String codigoCliente = "";

    private String idCertisign = "";
    private String chaveCertisign = "";
    private String tipo = "";

    public ArquivoPropostaStecVO() {
        super();
    }

    public void setCodigoArquivo(String codigoArquivo) {
        this.codigoArquivo = codigoArquivo;
    }

    public String getCodigoArquivo() {
        return codigoArquivo;
    }

    public void setTituloArquivo(String tituloArquivo) {
        this.tituloArquivo = tituloArquivo;
    }

    public String getTituloArquivo() {
        return tituloArquivo;
    }

    public void setNomeArquivo(String nomeArquivo) {
        this.nomeArquivo = nomeArquivo;
    }

    public String getNomeArquivo() {
        return nomeArquivo;
    }

    public void setAnexo(byte[] anexo) {
        this.anexo = anexo;
    }

    public byte[] getAnexo() {
        return anexo;
    }

    public void setCodigoProposta(String codigoProposta) {
        this.codigoProposta = codigoProposta;
    }

    public String getCodigoProposta() {
        return codigoProposta;
    }

    public String getCodigoEntidade() {
        return codigoEntidade;
    }

    public void setCodigoEntidade(String codigoEntidade) {
        this.codigoEntidade = codigoEntidade;
    }

    public String getCodigoTipoTermo() {
        return codigoTipoTermo;
    }

    public void setCodigoTipoTermo(String codigoTipoTermo) {
        this.codigoTipoTermo = codigoTipoTermo;
    }

    public String getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public int getTamanhoArquivo() {
        return tamanhoArquivo;
    }

    public void setTamanhoArquivo(int tamanhoArquivo) {
        this.tamanhoArquivo = tamanhoArquivo;
    }

    public String getIdCertisign() {
        return idCertisign;
    }

    public void setIdCertisign(String idCertisign) {
        this.idCertisign = idCertisign;
    }

    public String getChaveCertisign() {
        return chaveCertisign;
    }

    public void setChaveCertisign(String chaveCertisign) {
        this.chaveCertisign = chaveCertisign;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCodigoStatusTermo() {
        return codigoStatusTermo;
    }

    public void setCodigoStatusTermo(String codigoStatusTermo) {
        this.codigoStatusTermo = codigoStatusTermo;
    }

}
