package br.com.sebrae.pr.sebraetec.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.sebrae.pr.sebraetec.api.dto.ArquivoPropostaStecVO;
import br.com.sebrae.pr.sebraetec.api.dto.EnvioAssinaturaDTO;
import br.com.sebrae.pr.sebraetec.api.service.CertisignService;
import br.com.wise.boot.starter.rest.BaseRS;

@RestController
@RequestMapping("/certisign")
public class CertisignController extends BaseRS {

    @Autowired
    private CertisignService certisignService;
    
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ArquivoPropostaStecVO envioAssinatura(@RequestBody EnvioAssinaturaDTO envioAssinaturaDTO ) throws Exception {
    	
    	return certisignService.envioAssinaturaCertisignApi(envioAssinaturaDTO.getAuth(), envioAssinaturaDTO.getRelatorio(), envioAssinaturaDTO.getArquivo());
    }
    
}
