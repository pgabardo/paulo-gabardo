package br.com.sebrae.pr.sebraetec.api.dto;


import java.util.ArrayList;
import java.util.List;

public class RelatorioVO {
    private String codigoProposta = "";
    private String local = "";
    private Agente solicitante = new Agente();
    private Agente responsavel = new Agente();
    private Agente gerente = new Agente();
    private Agente subGerente = new Agente();
    private Agente cliente = new Agente();
    private Agente contato = new Agente();
    private String razaoSocial = "";
    private String nomeFantasia = "";
    private String cnae = "";
    private String cnpj = "";
    private String inicioAtividades = "";
    private String faturamento = "";
    private String setor = "";
    private String porte = "";
    private String quantFuncionario = "";
    private String conclusaofinal = "";

    private String cargoContato = "";
    private String rgContato = "";
    // Projeto
    private String demanda = "";
    private String necessidade = "";

    // Entidade
    private Agente entidade = new Agente();
    private String razaoSocialEntidade = "";
    private String nomeFantasiaEntidade = "";
    private String cnpjEntidade = "";
    private Agente contatoEntidade = new Agente();
    private Agente coordenadorEntidade = new Agente();

    // Plano de Trabalho
    private String planoSituacaoAtual = "";
    private String planoNecessidade = "";
    private String planoResultado = "";
    private String planoEntregasPrevistas = "";

    // Diretores Aprovadores caso categoria precisa
    private int indPrecisaAprovacaoDiretor = 0;
    private Agente diretor1 = new Agente();
    private Agente diretor2 = new Agente();
    private Agente diretor3 = new Agente();

    private String indicadorInovacao = "";

    // Pacto do Termo
    private String valorProposta = "";
    private String valorTotalSebrae = "";
    private String valorTotalContrapartida = "";
    private String valorTotalContrapartidaCliente = "";
    private String valorTotalMenosContrapartidaSebraeNa = "";
    private String valorMedioClientes = "";
    private String parcelas = "";
    private String quantClientes = "";
    private String quantClientesPagantes = "";
    private String valorSubsidio = "";
    private String valorContrapartida = "";
    private String valorPatrocinador = "";
    // private String valorContrapartidaExtenso = "";
    private String percSebraePR = "";
    private String percContrapartida = "";

    private List listaAtividades = new ArrayList();
    private List<RelatorioVO> listaClientes = new ArrayList<RelatorioVO>();

    private ResumoFinanceiroVO resumoFinanceiro = new ResumoFinanceiroVO();

    // Novos campos do relat�rio
    private String categoria = "";
    private String temas = "";
    private String servicos = "";
    private String subservicos = "";

    private String codigoParceiro = "";
    private String nomeParceiro = "";

    private String codigoPatrocinador = "";
    private String nomePatrocinador = "";

    private String resultadoObtido = "";
    private String solucaoObtida = "";

    public RelatorioVO() {
        super();
    }

    public String getPorte() {
        return porte;
    }

    public void setPorte(String porte) {
        this.porte = porte;
    }

    public Agente getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(Agente responsavel) {
        this.responsavel = responsavel;
    }

    public void setCodigoProposta(String codigoProposta) {
        this.codigoProposta = codigoProposta;
    }

    public String getCodigoProposta() {
        return codigoProposta;
    }

    public void setCliente(Agente cliente) {
        this.cliente = cliente;
    }

    public Agente getCliente() {
        return cliente;
    }

    public void setSolicitante(Agente solicitante) {
        this.solicitante = solicitante;
    }

    public Agente getSolicitante() {
        return solicitante;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getNomeFantasia() {
        return nomeFantasia;
    }

    public void setCnae(String cnae) {
        this.cnae = cnae;
    }

    public String getCnae() {
        return cnae;
    }

    public void setInicioAtividades(String inicioAtividades) {
        this.inicioAtividades = inicioAtividades;
    }

    public String getInicioAtividades() {
        return inicioAtividades;
    }

    public void setContato(Agente contato) {
        this.contato = contato;
    }

    public Agente getContato() {
        return contato;
    }

    public void setFaturamento(String faturamento) {
        this.faturamento = faturamento;
    }

    public String getFaturamento() {
        return faturamento;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public String getSetor() {
        return setor;
    }

    public void setQuantFuncionario(String quantFuncionario) {
        this.quantFuncionario = quantFuncionario;
    }

    public String getQuantFuncionario() {
        return quantFuncionario;
    }

    public void setCargoContato(String cargoContato) {
        this.cargoContato = cargoContato;
    }

    public String getCargoContato() {
        return cargoContato;
    }

    public void setRgContato(String rgContato) {
        this.rgContato = rgContato;
    }

    public String getRgContato() {
        return rgContato;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getLocal() {
        return local;
    }

    public void setDemanda(String demanda) {
        this.demanda = demanda;
    }

    public String getDemanda() {
        return demanda;
    }

    public void setNecessidade(String necessidade) {
        this.necessidade = necessidade;
    }

    public String getNecessidade() {
        return necessidade;
    }

    public void setEntidade(Agente entidade) {
        this.entidade = entidade;
    }

    public Agente getEntidade() {
        return entidade;
    }

    public void setRazaoSocialEntidade(String razaoSocialEntidade) {
        this.razaoSocialEntidade = razaoSocialEntidade;
    }

    public String getRazaoSocialEntidade() {
        return razaoSocialEntidade;
    }

    public void setNomeFantasiaEntidade(String nomeFantasiaEntidade) {
        this.nomeFantasiaEntidade = nomeFantasiaEntidade;
    }

    public String getNomeFantasiaEntidade() {
        return nomeFantasiaEntidade;
    }

    public void setCnpjEntidade(String cnpjEntidade) {
        this.cnpjEntidade = cnpjEntidade;
    }

    public String getCnpjEntidade() {
        return cnpjEntidade;
    }

    public void setPlanoSituacaoAtual(String planoSituacaoAtual) {
        this.planoSituacaoAtual = planoSituacaoAtual;
    }

    public String getPlanoSituacaoAtual() {
        return planoSituacaoAtual;
    }

    public void setPlanoNecessidade(String planoNecessidade) {
        this.planoNecessidade = planoNecessidade;
    }

    public String getPlanoNecessidade() {
        return planoNecessidade;
    }

    public void setPlanoResultado(String planoResultado) {
        this.planoResultado = planoResultado;
    }

    public String getPlanoResultado() {
        return planoResultado;
    }

    public void setIndicadorInovacao(String indicadorInovacao) {
        this.indicadorInovacao = indicadorInovacao;
    }

    public String getIndicadorInovacao() {
        return indicadorInovacao;
    }

    public void setValorProposta(String valorProposta) {
        this.valorProposta = valorProposta;
    }

    public String getValorProposta() {
        return valorProposta;
    }

    public void setParcelas(String parcelas) {
        this.parcelas = parcelas;
    }

    public String getParcelas() {
        return parcelas;
    }

    public void setQuantClientes(String quantClientes) {
        this.quantClientes = quantClientes;
    }

    public String getQuantClientes() {
        return quantClientes;
    }

    public void setQuantClientesPagantes(String quantClientesPagantes) {
        this.quantClientesPagantes = quantClientesPagantes;
    }

    public String getQuantClientesPagantes() {
        return quantClientesPagantes;
    }

    public void setValorSubsidio(String valorSubsidio) {
        this.valorSubsidio = valorSubsidio;
    }

    public String getValorSubsidio() {
        return valorSubsidio;
    }

    public void setValorContrapartida(String valorContrapartida) {
        this.valorContrapartida = valorContrapartida;
    }

    public String getValorContrapartida() {
        return valorContrapartida;
    }

    public void setValorPatrocinador(String valorPatrocinador) {
        this.valorPatrocinador = valorPatrocinador;
    }

    public String getValorPatrocinador() {
        return valorPatrocinador;
    }

    public String getPercSebraePR() {
        return percSebraePR;
    }

    public void setPercSebraePR(String percSebraePR) {
        this.percSebraePR = percSebraePR;
    }

    public String getPercContrapartida() {
        return percContrapartida;
    }

    public void setPercContrapartida(String percContrapartida) {
        this.percContrapartida = percContrapartida;
    }

    public void setListaAtividades(List listaAtividades) {
        this.listaAtividades = listaAtividades;
    }

    public List getListaAtividades() {
        return listaAtividades;
    }

    public void setResumoFinanceiro(ResumoFinanceiroVO resumoFinanceiro) {
        this.resumoFinanceiro = resumoFinanceiro;
    }

    public ResumoFinanceiroVO getResumoFinanceiro() {
        return resumoFinanceiro;
    }

    public void setValorTotalSebrae(String valorTotalSebrae) {
        this.valorTotalSebrae = valorTotalSebrae;
    }

    public String getValorTotalSebrae() {
        return valorTotalSebrae;
    }

    public void setValorTotalContrapartida(String valorTotalContrapartida) {
        this.valorTotalContrapartida = valorTotalContrapartida;
    }

    public String getValorTotalContrapartida() {
        return valorTotalContrapartida;
    }

    public void setValorTotalContrapartidaCliente(String valorTotalContrapartidaCliente) {
        this.valorTotalContrapartidaCliente = valorTotalContrapartidaCliente;
    }

    public String getValorTotalContrapartidaCliente() {
        return valorTotalContrapartidaCliente;
    }

    public void setValorTotalMenosContrapartidaSebraeNa(String valorTotalMenosContrapartidaSebraeNa) {
        this.valorTotalMenosContrapartidaSebraeNa = valorTotalMenosContrapartidaSebraeNa;
    }

    public String getValorTotalMenosContrapartidaSebraeNa() {
        return valorTotalMenosContrapartidaSebraeNa;
    }

    public void setValorMedioClientes(String valorMedioClientes) {
        this.valorMedioClientes = valorMedioClientes;
    }

    public String getValorMedioClientes() {
        return valorMedioClientes;
    }

    // public void setValorContrapartidaExtenso(String valorContrapartidaExtenso) {
    // this.valorContrapartidaExtenso = valorContrapartidaExtenso;
    // }
    //
    // public String getValorContrapartidaExtenso() {
    // return valorContrapartidaExtenso;
    // }

    public void setContatoEntidade(Agente contatoEntidade) {
        this.contatoEntidade = contatoEntidade;
    }

    public Agente getContatoEntidade() {
        return contatoEntidade;
    }

    public void setCoordenadorEntidade(Agente coordenadorEntidade) {
        this.coordenadorEntidade = coordenadorEntidade;
    }

    public Agente getCoordenadorEntidade() {
        return coordenadorEntidade;
    }

    public void setConclusaofinal(String conclusaofinal) {
        this.conclusaofinal = conclusaofinal;
    }

    public String getConclusaofinal() {
        return conclusaofinal;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getTemas() {
        return temas;
    }

    public void setTemas(String temas) {
        this.temas = temas;
    }

    public String getServicos() {
        return servicos;
    }

    public void setServicos(String servicos) {
        this.servicos = servicos;
    }

    public String getSubservicos() {
        return subservicos;
    }

    public void setSubservicos(String subservicos) {
        this.subservicos = subservicos;
    }

    public String getCodigoParceiro() {
        return codigoParceiro;
    }

    public void setCodigoParceiro(String codigoParceiro) {
        this.codigoParceiro = codigoParceiro;
    }

    public String getNomeParceiro() {
        return nomeParceiro;
    }

    public void setNomeParceiro(String nomeParceiro) {
        this.nomeParceiro = nomeParceiro;
    }

    public String getResultadoObtido() {
        return resultadoObtido;
    }

    public void setResultadoObtido(String resultadoObtido) {
        this.resultadoObtido = resultadoObtido;
    }

    public String getSolucaoObtida() {
        return solucaoObtida;
    }

    public void setSolucaoObtida(String solucaoObtida) {
        this.solucaoObtida = solucaoObtida;
    }

    public Agente getGerente() {
        return gerente;
    }

    public void setGerente(Agente gerente) {
        this.gerente = gerente;
    }

    public Agente getSubGerente() {
        return subGerente;
    }

    public void setSubGerente(Agente subGerente) {
        this.subGerente = subGerente;
    }

    public String getCodigoPatrocinador() {
        return codigoPatrocinador;
    }

    public void setCodigoPatrocinador(String codigoPatrocinador) {
        this.codigoPatrocinador = codigoPatrocinador;
    }

    public String getNomePatrocinador() {
        return nomePatrocinador;
    }

    public void setNomePatrocinador(String nomePatrocinador) {
        this.nomePatrocinador = nomePatrocinador;
    }

    public List<RelatorioVO> getListaClientes() {
        return listaClientes;
    }

    public void setListaClientes(List<RelatorioVO> listaClientes) {
        this.listaClientes = listaClientes;
    }

    public int getIndPrecisaAprovacaoDiretor() {
        return indPrecisaAprovacaoDiretor;
    }

    public void setIndPrecisaAprovacaoDiretor(int indPrecisaAprovacaoDiretor) {
        this.indPrecisaAprovacaoDiretor = indPrecisaAprovacaoDiretor;
    }

    public Agente getDiretor1() {
        return diretor1;
    }

    public void setDiretor1(Agente diretor1) {
        this.diretor1 = diretor1;
    }

    public Agente getDiretor2() {
        return diretor2;
    }

    public void setDiretor2(Agente diretor2) {
        this.diretor2 = diretor2;
    }

    public Agente getDiretor3() {
        return diretor3;
    }

    public void setDiretor3(Agente diretor3) {
        this.diretor3 = diretor3;
    }

	public String getPlanoEntregasPrevistas() {
		return planoEntregasPrevistas;
	}

	public void setPlanoEntregasPrevistas(String planoEntregasPrevistas) {
		this.planoEntregasPrevistas = planoEntregasPrevistas;
	}
}
