package br.com.sebrae.pr.sebraetec.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.sebrae.pr.sebraetec.api.dto.AprovarPagarPropostaDTO;
import br.com.sebrae.pr.sebraetec.api.dto.AtividadePropostaDTO;
import br.com.sebrae.pr.sebraetec.api.dto.PropostaDTO;
import br.com.sebrae.pr.sebraetec.api.dto.RecusarPropostaDTO;
import br.com.sebrae.pr.sebraetec.api.service.PropostaService;
import br.com.wise.boot.starter.rest.BaseRS;

@RestController
@RequestMapping("/proposta")
public class PropostaController extends BaseRS {

    @Autowired
    private PropostaService propostaService;

    @RequestMapping(method = RequestMethod.GET, path = "/{codCliente}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<PropostaDTO> pesquisarPorCodigoCliente(@PathVariable final Long codCliente) {
        return propostaService.pesquisarPorCodigoCliente(codCliente);
    }
    
    @RequestMapping(method = RequestMethod.GET, path = "/{codProposta}/atividades", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AtividadePropostaDTO> pesquisarAtividades(@PathVariable final Long codProposta) {
        return propostaService.pesquisarAtividadesProposta(codProposta);
    }

    
    @RequestMapping(path = "/recusar", method = RequestMethod.PUT)
    @ResponseBody
    public void recusarProposta(@RequestBody final RecusarPropostaDTO recusarProposta) {
        propostaService.recusarProposta(recusarProposta.getCodProposta(), recusarProposta.getCodCliente(), recusarProposta.getJustificativa());
    }
    
    @RequestMapping(path = "/aprovar", method = RequestMethod.PUT)
    @ResponseBody
    public void aprovarProposta(@RequestBody final AprovarPagarPropostaDTO aprovarProposta) {
        propostaService.aprovarProposta(aprovarProposta.getCodProposta(), aprovarProposta.getCodCliente());
    }
    
    @RequestMapping(path = "/plano-trabalho/recusar", method = RequestMethod.PUT)
    @ResponseBody
    public void recusarPlanoTrabalho(@RequestBody final RecusarPropostaDTO recusarProposta) {
        propostaService.recusarPlanoTrabalho(recusarProposta.getCodProposta(), recusarProposta.getCodCliente(), recusarProposta.getJustificativa());
    }
    
    @RequestMapping(path = "/plano-trabalho/aprovar", method = RequestMethod.PUT)
    @ResponseBody
    public void aprovarPlanoTrabalho(@RequestBody final AprovarPagarPropostaDTO aprovarProposta) {
        propostaService.aprovarPlanoTrabalho(aprovarProposta.getCodProposta(), aprovarProposta.getCodCliente(), aprovarProposta.isFinalizacao());
    }

}
