package br.com.sebrae.pr.sebraetec.api.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import br.com.sebrae.pr.core.util.exceptions.ExcecaoDeInfraEstrutura;

public class PropertyUtil {
	private static final Logger LOGGER = Logger.getLogger(PropertyUtil.class);

	private static Properties properties = null;

	private PropertyUtil() {
		super();
	}

	/**
	 * Obt�m determinada propriedade.
	 * 
	 * @param property
	 * @return
	 */
	public static String getProperty(final String property) {
		if (StringUtils.isBlank(property)) {
			return null;
		}
		if (getProperties() == null) {
			return property;
		}

		final String retorno = getProperties().getProperty(property);

		if (StringUtils.isBlank(retorno)) {
			LOGGER.error("Propriedade n�o encontrada no arquivo application.properties. Propriedade: '" + property + "'.");
		}

		return retorno;
	}

	private static Properties getProperties() {
		if (properties == null) {
			InputStream is = null;
			try {
				properties = new Properties();
				is = PropertyUtil.class.getClassLoader().getResourceAsStream("application.properties");
				properties.load(is);

			} catch (Exception ex) {
				throw new ExcecaoDeInfraEstrutura(ex);

			} finally {
				if (is != null) {
					try {
						is.close();
					} catch (IOException e) {
						LOGGER.error("Erro fechando inputStream do application.properties", e);
					}
				}
			}
		}

		return properties;
	}

	public static String getBuildVersion() {
		return getProperty("mvn.build.version");
	}

	public static String getBuildTimestamp() {
		return getProperty("mvn.build.timestamp");
	}

	public static String getBuildProfile() {
		return getProperty("mvn.build.profile");
	}

	public static String getRetWsSerializationDir() {
		return getProperty("app.retWS.serialization.dir");
	}

	public static boolean isBuildProfileDesenvolvimento() {
		return "desenvolvimento".equalsIgnoreCase(getBuildProfile());
	}

}
