package br.com.sebrae.pr.sebraetec.api.dto;

import java.io.Serializable;

public class RecusarPropostaDTO implements Serializable {

	private static final long serialVersionUID = -5726398863199195674L;
	
	private Long codCliente;    
    private Long codProposta;
    private String justificativa;
    
    public RecusarPropostaDTO() {
    }

	public Long getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(Long codCliente) {
		this.codCliente = codCliente;
	}

	public Long getCodProposta() {
		return codProposta;
	}

	public void setCodProposta(Long codProposta) {
		this.codProposta = codProposta;
	}

	public String getJustificativa() {
		return justificativa;
	}

	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}
    	
}
