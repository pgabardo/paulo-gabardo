package br.com.sebrae.pr.sebraetec.api.dao;

import org.springframework.stereotype.Service;

import br.com.sebrae.pr.sebraetec.api.dto.ArquivoPropostaStecVO;

@Service
public interface CertisignDAO {

	public ArquivoPropostaStecVO setTermoProvisorio(ArquivoPropostaStecVO doc, String codigoAgenteAtz) throws Exception;
	
}
